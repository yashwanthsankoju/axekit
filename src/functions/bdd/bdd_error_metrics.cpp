// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : bdd_error_metrics.cpp
// @brief  : functions to report error metrics using bdd.
//------------------------------------------------------------------------------

#include "bdd_error_metrics.hpp"
#include <utils/nanotrav/blif_nanotrav_io.hpp>

namespace axekit
{

using mpfloat = boost::multiprecision::cpp_dec_float_100;
using mpuint = boost::multiprecision::uint256_t;

mpuint worst_case ( const bdd_function_t &fs, const bdd_function_t &fshat ) {
  //---// The below logic is wrong. Since the max-values for each can be completely different.
  //---// Only in combination there is a meaning.
  //---if ( get_max_value (fs) > get_max_value (fshat) ) {
  //---  return get_max_value ( (subtract_bdd (fs, fshat)).first  );
  //---}
  //---else {
  //---  return get_max_value ( (subtract_bdd (fshat, fs)).first );
  //---}
  
  auto abs = abs_bdd ( subtract_bdd (fs, fshat));
  return get_max_value ( abs );
}

mpuint worst_case_chi ( const bdd_function_t &fs, const bdd_function_t &fshat ) {
  //---// The below logic is wrong. Since the max-values for each can be completely different.
  //---// Only in combination there is a meaning.
  //---if ( get_max_value (fs) > get_max_value (fshat) ) {
  //---  return get_max_value_chi ( (subtract_bdd (fs, fshat)).first  );
  //---}
  //---else {
  //---  return get_max_value_chi ( (subtract_bdd (fshat, fs)).first );
  //---}

  const auto subtr = subtract_bdd (fs, fshat);
  auto abs = abs_bdd ( subtr );
  return get_max_value_chi ( abs );
}

mpfloat average_case ( const bdd_function_t &fs, const bdd_function_t &fshat ) {

  //bdd_function_t diff;
  //---if ( get_max_value (fs) > get_max_value (fshat) ) {
  //---  diff = (subtract_bdd (fs, fshat)).first;
  //---}
  //---else {
  //---  diff = (subtract_bdd (fshat, fs)).first;
  //---}
  
  const auto subtr = subtract_bdd (fs, fshat);
  auto diff =  abs_bdd ( subtr );
  const boost::multiprecision::uint256_t one = 1;
  auto sum = mpfloat ( get_weighted_sum (diff) );
  //std::cout << "Sum = " << sum  << "\n";
  auto num_inputs = fs.first.ReadSize();
  return sum / mpfloat ( one << num_inputs );
}

mpfloat sum_of_errors ( const bdd_function_t &diff ) {
  return mpfloat ( get_weighted_sum (diff) );
}

mpuint error_rate ( const bdd_function_t& f, const bdd_function_t& fhat ) {

  auto h = f.first.bddZero();

  for ( auto i = 0u; i < f.second.size(); ++i )
  {
    h |= f.second[i] ^ fhat.second[i];
  }

  std::stringstream s;
  s.precision(0);
  s << std::fixed << h.CountMinterm( f.first.ReadSize() );
  return boost::multiprecision::uint256_t( s.str() );

  //const auto counts = count_solutions( h.manager(), {h.getNode()} );
  //return get_count( h.manager(), h.getNode(), counts ) << h.getNode()->index;
}

mpuint bit_flip_error (const bdd_function_t &fs, const bdd_function_t &fshat) {
  
  auto fsum = add_individual_outputs ( xor_bdd (fs, fshat) );
  return get_max_value (fsum);
  
}


mpuint bit_flip_error_chi (const bdd_function_t &fs, const bdd_function_t &fshat) {
  
  auto fsum = add_individual_outputs ( xor_bdd (fs, fshat) );
  return get_max_value_chi (fsum);
  
}

} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
