// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_countsat.cpp
// @brief  : CounSat procedures with Gia
//------------------------------------------------------------------------------


#include "gia_countsat.hpp"
#include <cstdio>
#include <proof/cec/cec.h>

// Note: There is something strange with this header.
// If this is included, abc::internal solver gives wrong results with double negation.
// Very interesting behaviour. To debug later.
//#include <ext-libs/cms5/scalmc.h>

#include <ext-libs/sharpsat/sharpsat.hpp>

namespace axekit
{

// Make sure after everything, the CMS headers are not used.
#ifdef CUSP_H_
#error "CUSP/CMS is incompatible with abc::sat_solver_solve_internal()"
#endif

//using mpuint = boost::multiprecision::uint256_t ;
using mpuint = boost::multiprecision::uint256_t;

// Routine from gia_lexsat.cpp. Do not include the full header here.
boost::dynamic_bitset<> solve_max_lexsat ( abc::Gia_Man_t *gia ); 

// The local functions in this file.
void undo_solver_assumptions ( abc::sat_solver *solver, const unsigned num_assumptions );
mpuint countsat_for_curr_assumption ( abc::sat_solver *solver, const int cnf_nVars, const int nPis );
std::pair<bool, unsigned> sat_solver_solve_with_assumptions ( abc::Gia_Man_t *gia, abc::sat_solver *solver,
							      const boost::dynamic_bitset<> &out_pattern );
void print_input_assignment ( abc::sat_solver * solver, const int nVars, const int nPis );
std::vector<int> get_input_assignment ( abc::sat_solver * solver, const int cnf_nVars, const int nPis );
bool is_var_value_x ( abc::sat_solver * solver, int var );
std::pair <boost::dynamic_bitset<>, unsigned> solve_max_lexsat_int ( abc::sat_solver * solver, abc::Gia_Man_t * gia,
								     const int num_po_bits ) ;

inline mpuint bs_to_mpuint ( const boost::dynamic_bitset<> &bs ) {
  mpuint sum(0);
  if ( bs.test(0) ) sum = 1;
  const mpuint two(2);
  for (auto i=1u; i <bs.size(); i++) { // bs does not hav iterators?
    if ( bs.test(i) ) sum = sum + (two << i);
  }
  return sum;
  
}


// All outputs are constrained to the respective value in output_pattern.
mpuint countsat_with_sharpsat ( abc::Gia_Man_t *gia,
				const boost::dynamic_bitset <> &output_pattern ) {

  auto solver = new SharpSATSolver();
  auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  const auto fReadable = 1;
  const auto num_outputs = abc::Gia_ManPoNum(gia);
  const auto num_inputs = abc::Gia_ManPiNum(gia);
  auto unused_inputs = solver->writeCnfData ( cnf, fReadable, num_inputs, num_outputs,
					      output_pattern );
  abc::Cnf_DataFree( cnf );

  const mpuint one (1);
  mpuint result = one << unused_inputs ;
  solver->solve ();
  if ( solver->status() )  return result * mpuint ( solver->model_count().get_ui() );
  return mpuint(0);
}

// All outputs are constrained to be true in the CNF.
// If some outputs are 0 constants or to be constrained as 0, use above routine.
mpuint countsat_with_sharpsat ( abc::Gia_Man_t *gia ) {
  auto solver = new SharpSATSolver();
  auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  const auto fReadable = 1;
  const auto num_outputs = abc::Gia_ManPoNum(gia);
  const auto num_inputs = abc::Gia_ManPiNum(gia);
  auto unused_inputs = solver->writeCnfData ( cnf, fReadable, num_inputs, num_outputs );
  abc::Cnf_DataFree( cnf );

  const mpuint one (1);
  mpuint result = one << unused_inputs ;
  solver->solve ();
  if ( solver->status() )  return result * mpuint ( solver->model_count().get_ui() );
  return mpuint(0);
}



bool block_input_literals ( abc::sat_solver *solver, int *lits, int num_lits ) {
  assert (false && "not yet ready");
  
  return true;
}


void print_input_assignment ( abc::sat_solver * solver, const int nVars, const int nPis ) {

  std::cout << "Input assignment = ";
  for (auto i = nPis-1; i >= 0; i--) {
    std::cout << abc::sat_solver_var_value ( solver, nVars - nPis + i );
  }
  std::cout << "\n";
}

std::vector<int> get_input_assignment ( abc::sat_solver * solver, const int cnf_nVars, const int nPis ) {
  std::vector<int> result (nPis);
  for (auto i = nPis-1; i >= 0; i--) {
    result[i] = abc::sat_solver_var_value ( solver, cnf_nVars - nPis + i );
  }
  return result;
}

// the var should be an INPUT PI var
// under the given assumptions, if both polarities are SAT, then var value is x.
// solver status is unchanged after this routine.
bool is_var_value_x ( abc::sat_solver * solver, int var ) {
  auto lit = abc::sat_solver_var_literal (solver, var);
  bool is_x = true;
  if ( !abc::sat_solver_push (solver,  abc::Abc_LitNot(lit) ) ) is_x = false;
  abc::sat_solver_pop (solver);
  return is_x;
}


void undo_solver_assumptions ( abc::sat_solver *solver, const unsigned num_assumptions ) 
{
  // Revert solver by the given number of assumptions.
  int i=0;
  while ( i < num_assumptions ) {
    abc::sat_solver_pop (solver);
    i++;
  }
}



// Find the max bit assignment for ONLY the given number of nPos from the MSB.
// None of the assumptions in the solver are reverted to the original state by the routine.
// Instead a value is returned giving the number of assumptions.
std::pair <boost::dynamic_bitset<>, unsigned>
solve_max_lexsat_int ( abc::sat_solver * solver, abc::Gia_Man_t * gia, const int num_po_bits ) {

  auto num_bits = num_po_bits;
  // set the initial polarity of the solver to max value.
  int polarity[num_bits] = {1};
  std::fill_n (polarity, num_bits, 1);
  boost::dynamic_bitset<> bs (num_bits); // result bitset.

  auto num_assumptions = 0u;
  
  // 1. prune the const outputs.
  // set polarity for const-0 correctly. correct assumptions on const outputs and result bitset 
  for (auto i=1; i<=num_bits; i++) { // Runs from 1 to num_bits (num_bits <= MSB)
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) )  {
      polarity[i-1] = 0;
      // definite way to get a 0!
      auto ll = abc::Abc_LitNot( abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ); 
      if ( !abc::sat_solver_push (solver, ll) ) { // while pushing itself, its UNSAT
	assert (false && "Const 0 assumption invalidated.");
      }
      bs.set (i-1, false);
      num_assumptions++;
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) )  {
      // definite way to get a 1!
      auto ll = abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ; 
      if ( !abc::sat_solver_push (solver, ll) ) { // while pushing itself, its UNSAT
	assert (false && "Const 1 assumption invalidated.");
      }
      bs.set (i-1, true);
      num_assumptions++;
    }
  }

  // 2. set the polarity for the sat-solver
  abc::sat_solver_set_literal_polarity (solver, polarity, num_bits);

  // 3. solve from the outputs.
  for (auto i=bs.size(); i>0 ; i--) { // start from the MSB
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) ) continue; 
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) ) continue; 

    int lit = abc::Abc_Var2Lit (i, 0); // put 1 first.
    bs.set (i-1, true);
    if ( !abc::sat_solver_push (solver, lit) ) { // while pushing itself, its UNSAT
      abc::sat_solver_pop (solver); // revert the last literal.
      bs.set (i-1, false); 
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	abc::sat_solver_pop (solver);
    	assert (false && "inversion cannot be UNSAT [1]");
      }
      continue;  // no need to solve further.
    }
    
    auto status = abc::sat_solver_solve_internal (solver);
    
    if (status == abc::l_True) continue; // assumption holds. Check nxt bit.
    if ( status == abc::l_False ) { // assumption does not hold. Revert and push the opposite
      abc::sat_solver_pop (solver); // revert the last literal.
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0.
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	assert (false && "inversion cannot be UNSAT [2]");
      }
      bs.set (i-1, false); 
    }
    else {
      assert (false && "Solver status undef");
    }
    num_assumptions++; // every iteration, net effect is one assumption.
  }

  return {bs, num_assumptions};
  
}


// Returns the max value on outputs and the input count for it.
// This is a temporary routine created just for convenience.
std::pair <boost::dynamic_bitset<>, mpuint>
solve_max_lexsat_with_count ( abc::Gia_Man_t * gia ) {
  assert (false); // Has bugs/interference with something else.
  // Note: this routine is buggy only in some cases!.
  // e.g. is the adders RCA_N16.v.aig and appx_4096_5.blif.aig
  // Here the results are different. Not debugging for the time being.
  
  const auto nPis = abc::Gia_ManPiNum (gia);
  auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  auto solver = (abc::sat_solver *)abc::Cnf_DataWriteIntoSolver( cnf, 1, 0 );
  const auto num_po_bits = abc::Gia_ManPoNum (gia);
  auto nVars = cnf->nVars;
  abc::Cnf_DataFree( cnf );
  
  auto num_bits = num_po_bits;
  // set the initial polarity of the solver to max value.
  int polarity[num_bits] = {1};
  std::fill_n (polarity, num_bits, 1);
  boost::dynamic_bitset<> bs (num_bits); // result bitset.

  // 1. prune the const outputs.
  // set polarity for const-0 correctly. correct assumptions on const outputs and result bitset 
  for (auto i=1; i<=num_bits; i++) { // Runs from 1 to num_bits (num_bits <= MSB)
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) )  {
      polarity[i-1] = 0;
      auto ll = abc::Abc_LitNot( abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ); 
      if ( !abc::sat_solver_push (solver, ll) ) assert (false); //Const 0 UNSAT
      bs.set (i-1, false);
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) )  {
      auto ll = abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ; 
      if ( !abc::sat_solver_push (solver, ll) ) assert (false); // Const 1 UNSAT
      bs.set (i-1, true);
    }
  }

  // 2. set the polarity for the sat-solver
  abc::sat_solver_set_literal_polarity (solver, polarity, num_bits);

  // 3. solve from the outputs.
  for (auto i=bs.size(); i>0 ; i--) { // start from the MSB
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) ) continue; 
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) ) continue; 

    int lit = abc::Abc_Var2Lit (i, 0); // put 1 first.
    bs.set (i-1, true);
    if ( !abc::sat_solver_push (solver, lit) ) { // while pushing itself, its UNSAT
      abc::sat_solver_pop (solver); // revert the last literal.
      bs.set (i-1, false); 
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	abc::sat_solver_pop (solver);
    	assert (false && "inversion cannot be UNSAT [1]");
      }
      continue;  // no need to solve further.
    }
    
    auto status = abc::sat_solver_solve_internal (solver);
    
    if (status == abc::l_True) continue; // assumption holds. Check nxt bit.
    if ( status == abc::l_False ) { // assumption does not hold. Revert and push the opposite
      abc::sat_solver_pop (solver); // revert the last literal.
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0.
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	assert (false && "inversion cannot be UNSAT [2]");
      }
      bs.set (i-1, false); 
    }
    else {
      assert (false && "Solver status undef");
    }
  }

  auto curr_count = countsat_for_curr_assumption (solver, nVars, nPis); // no:of input assignments

  abc::sat_solver_delete (solver);
  return {bs, curr_count};
}


boost::dynamic_bitset<> experimental_solve_maxlexsat_int ( abc::Gia_Man_t *gia ) {
  
  auto num_bits = abc::Gia_ManPoNum (gia);
  auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  auto * solver = (abc::sat_solver *)abc::Cnf_DataWriteIntoSolver( cnf, 1, 0 );
  //char cnffile[] = "max.cnf";
  //abc::Mf_ManDumpCnf ( gia, cnffile, 8, 0, 0, 0 );
  const auto nVars = cnf->nVars;
  const auto nPis = abc::Gia_ManPiNum (gia);
  abc::Cnf_DataFree( cnf );

  // set the initial polarity of the solver to max value.
  int polarity[num_bits] = {1};
  std::fill_n (polarity, num_bits, 1);
  //int polarity[num_bits] = { [0 ... num_bits-1] = 1 }; // Only GCC syntax

  boost::dynamic_bitset<> bs (num_bits); // result bitset.

  // 1. prune the const outputs.
  // set polarity for const-0 correctly. correct assumptions on const outputs and result bitset 
  for (auto i=1; i<=num_bits; i++) {
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) )  {
      polarity[i-1] = 0;
      // definite way to get a 0!
      auto ll = abc::Abc_LitNot( abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ); 
      if ( !abc::sat_solver_push (solver, ll) ) { // while pushing itself, its UNSAT
	assert (false && "Const 0 assumption invalidated.");
      }
      bs.set (i-1, false);
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) )  {
      // definite way to get a 1!
      auto ll = abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ; 
      if ( !abc::sat_solver_push (solver, ll) ) { // while pushing itself, its UNSAT
	assert (false && "Const 1 assumption invalidated.");
      }
      bs.set (i-1, true);
    }
  }
  
  // 2. set the polarity for the sat-solver
  abc::sat_solver_set_literal_polarity (solver, polarity, num_bits);

  // 3. solve from the outputs.
  for (auto i=bs.size(); i>0 ; i--) { // start from the MSB
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) ) continue; 
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) ) continue; 

    int lit = abc::Abc_Var2Lit (i, 0); // put 1 first.
    bs.set (i-1, true);
    if ( !abc::sat_solver_push (solver, lit) ) { // while pushing itself, its UNSAT
      abc::sat_solver_pop (solver); // revert the last literal.
      bs.set (i-1, false); 
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	abc::sat_solver_pop (solver);
    	assert (false && "inversion cannot be UNSAT [1]");
      }
      continue;  // no need to solve further.
    }
    
    auto status = abc::sat_solver_solve_internal (solver);
    
    if (status == abc::l_True) continue; // assumption holds. Check nxt bit.
    if ( status == abc::l_False ) { // assumption does not hold. Revert and push the opposite
      abc::sat_solver_pop (solver); // revert the last literal.
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0.
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	assert (false && "inversion cannot be UNSAT [2]");
      }
      bs.set (i-1, false); 
    }
    else {
      assert (false && "Solver status undef");
    }

  }

  auto vals = get_input_assignment (solver, nVars, nPis);
  std::cout << "Input assignment = ";
  for (auto i = nPis - 1; i >= 0; i--) {
    if ( is_var_value_x ( solver, nVars - nPis + i ) ) std::cout << "X";
    else std::cout << vals[i];
  }
  std::cout << "\n";

  
  sat_solver_delete (solver);
  //std::cout << "Max = "  << bs << "\n";
  return bs;
}



mpuint countsat_for_curr_assumption ( abc::sat_solver *solver, const int cnf_nVars, const int nPis ) {
  const mpuint two (2);
  mpuint result = 1;
  for (auto i=nPis - 1; i >= 0; i--) {
    if ( is_var_value_x ( solver, cnf_nVars - nPis + i ) )
      result = result * two;
  }
  return result;
}


inline void bs_set_pattern ( boost::dynamic_bitset<> &bs, mpuint &val ) {
  bs.reset();  // all set to 0
  mpuint bit = 0;
  const mpuint one(1);
  const mpuint zero(0);
  for (auto i=0u; i<bs.size(); i++) {
    bit = (val >> 1) & one;
    val = (val >> 1);
    if (bit == one)
      bs.set (i, true);
    else
      bs.set (i, false);
    if (val == zero) return; // no need to set further.
  }
}


inline boost::dynamic_bitset<> next_max_pattern ( const boost::dynamic_bitset<> &pattern ) {
  //auto val = pattern.to_ulong();
  auto val = bs_to_mpuint (pattern) - 1 ;
  if (val > 1) {
    auto bs = boost::dynamic_bitset<> ( pattern.size() );
    bs_set_pattern ( bs, val );
    return bs;
  }
  return boost::dynamic_bitset<> ( pattern.size(), 0); // return all 0s.
}


// The pattern is ONLY for the output literals.
// The routine restores the solver state only if it is UNSAT.
// Otherwise it returns the number of succesfull assumptions.
std::pair<bool, unsigned> sat_solver_solve_with_assumptions (
  abc::Gia_Man_t *gia, abc::sat_solver *solver,  const boost::dynamic_bitset<> &out_pattern ) {
  
  auto num_assumptions = 0;
  bool unsat = false;
    
  // 1. Put all the assumptions first.
  for (auto i=out_pattern.size(); i>0 ; i--) { // start from the MSB
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) ) continue; 
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) ) continue; 

    auto lit1 = abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ); // Literal 1
    auto lit0 = abc::Abc_LitNot( abc::Abc_LitRegular( lit1 ) ); // Literal 0
    if ( out_pattern.test (i-1) ) { // assumption 1
      if ( !abc::sat_solver_push (solver, lit1) ) { // while pushing itself, its UNSAT
	abc::sat_solver_pop (solver);
	unsat = true;
	//std::cout << "push 1 \n";
	break; 
      }
    }
    else { // assumption 0.
      if ( !abc::sat_solver_push (solver, lit0) ) { // while pushing itself, its UNSAT
	abc::sat_solver_pop (solver);
	unsat = true;
	//std::cout << "push 0 in " << i << " the var\n";
	break;
      }
    }
    num_assumptions++;
  }

    
  if (unsat) {
    //std::cout << "Unsat while setting assumptions\n\n";
    for (auto i=0; i<num_assumptions; i++) // clean the solver and return.
      abc::sat_solver_pop (solver);
    return {false, 0};
  }

  // 2. solve with assumptions
  auto status = abc::sat_solver_solve_internal (solver);
    
  if (status == abc::l_True)  return {true, num_assumptions};  // assumption holds.
  
  if ( status == abc::l_False ) { // assumption does not hold. Clean the solver and return.
    for (auto i=0; i<num_assumptions; i++)
      abc::sat_solver_pop (solver);
    return {false, 0};
  }
  else {
    assert (false && "Solver status undef");
  }

  assert (false); // should never come here.
  return {false, 0}; 
  
}

inline bool test_output_consts ( abc::Gia_Man_t *gia, const boost::dynamic_bitset<> &bs ) {
  for (auto i=1; i<=bs.size(); i++) { // Runs from MSB
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) )  { 
      if ( bs.test(i-1) ) return false; // 1 for 0
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) )  {
      if ( !bs.test(i-1) ) return false; // 0 for 1

    }
  }
  return true;
}



// Find the total error of num_max_values
// e.g.
// Top 5 max values are 1101, 1100, 1000, 0111, 0101 and corresponding input counts  1, 2, 3, 4, 5
// sum_of_max_values (gia, 5) = 13*1 + 12*2 + 8*3 + 7*4 + 5*5 = 114
// Max values are found using LEXSAT procedure.
mpuint sum_of_max_values ( abc::Gia_Man_t *gia, const mpuint &num_max_values, const unsigned tool_type ) {

  assert (tool_type == 1u); // Only sharpsat is supported.

  mpuint konstant = 0;
  const mpuint one(1);
  auto konst_count = 0;
  for (auto i=0; i<abc::Gia_ManPoNum(gia); i++) {
    if ( abc::Gia_ManPoIsConst0 (gia, i) ) konst_count++;
    if ( abc::Gia_ManPoIsConst1 (gia, i) ) {
      konst_count++;
      konstant = konstant + (one << i);
    }
  }
  if ( konst_count == abc::Gia_ManPoNum(gia) )  {// every output is a constant.
    // every input combination, output is const. (konstant can be 0 also.)
    return konstant * ( one << abc::Gia_ManPiNum(gia) );
  }

  // No luck, need to use SAT solvers now.
  
  // patterns can be generated, counted and added on the fly. Splitting for debugging purpose.
  // TODO: Once stable, remove these vectors.
  std::vector< boost::dynamic_bitset<> > patterns;
  std::vector< mpuint > counts;
  
  int nVars;
  auto num_bits = abc::Gia_ManPoNum (gia);
  const auto nPis = abc::Gia_ManPiNum (gia);
  // 1. find the max value and get all the input assignments.
  mpuint curr_count;
  boost::dynamic_bitset<> curr_pattern;
  if ( 0u == tool_type ) {
    auto res = solve_max_lexsat_with_count ( gia ); // max val output pattern + count
    curr_pattern = res.first;
    curr_count = res.second;
  }
  else if ( 1u == tool_type ) {
    curr_pattern = solve_max_lexsat (gia); // this routine is from gia_lexsat.hpp
    curr_count = countsat_with_sharpsat (gia, curr_pattern); // no:of input assignments
  }
  else {
    assert (false && "Unsupported tool type!");
  }
  patterns.push_back ( curr_pattern );
  counts.push_back ( curr_count );

  // TODO: debug here.
  // Somehow the solver is inconsistent after this.
  // Current solution : use solve_max_lexsat_tmp instead of solve_max_lexsat_int, i.e. delete and recreate the solver.
  // Commit  07af304c9a269b81 has the original intended form of this routine.
  auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  auto solver = (abc::sat_solver *)abc::Cnf_DataWriteIntoSolver( cnf, 1, 0 );
  nVars = cnf->nVars;
  abc::Cnf_DataFree( cnf );

  // set the initial polarity of the solver to max value.
  int polarity[num_bits] = {1};
  std::fill_n (polarity, num_bits, 1);

  
  // 2. prune the const outputs and set these assumptions.
  for (auto i=1; i<=num_bits; i++) { // Runs from 1 to num_bits (num_bits <= MSB)
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) )  {
      polarity[i-1] = 0;
      auto ll = abc::Abc_LitNot( abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ); 
      if ( !abc::sat_solver_push (solver, ll) ) assert (false); // Const 0 assumption invalidated
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) )  {
      polarity[i-1] = 1;
      auto ll = abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ; 
      if ( !abc::sat_solver_push (solver, ll) ) assert (false); // Const 1 assumption invalidated.
    }
  }

  abc::sat_solver_set_literal_polarity (solver, polarity, num_bits);

  // 3. find the next 100 max-patterns
  // Some learning opportunities are discarded for simplicity.
  // TODO: rewrite this routine completely.
  mpuint k = 0;
  //std::cout << "curr_pattern size = " << curr_pattern.size() << "\n";

  //auto last_val = curr_pattern.to_ulong();
  auto last_val = bs_to_mpuint (curr_pattern);
  while ( k < num_max_values ) { // Max number of patterns is num_max_values
    curr_pattern = next_max_pattern ( curr_pattern );
    //std::cout << "Next max pattern = " << curr_pattern << "\n";
    if ( curr_pattern.none() ) break;
    //if ( curr_pattern.to_ulong() == last_val ) break; // constant output pattern looping.
    if ( bs_to_mpuint (curr_pattern) == last_val ) break; // constant output pattern looping.
    // last_val = curr_pattern.to_ulong();
    last_val = bs_to_mpuint (curr_pattern);
    // remove the pattern with false assumptions on const outputs.
    if ( !test_output_consts ( gia, curr_pattern ) ){
      //std::cout << "Const test failed\n";
      continue; // constants are modified.
    }
      
    // this is a valid one. Check SAT or not.
    auto result = sat_solver_solve_with_assumptions ( gia, solver, curr_pattern );
    if (!result.first) {
      //std::cout << "Pattern is UNSAT\n";
      continue; // UNSAT 
    }
    if ( tool_type == 0u)  // aXc / abc internal solver.
      curr_count = countsat_for_curr_assumption (solver, nVars, nPis); // no:of input assignments
    else if ( tool_type == 1u ) // sharpsat
      curr_count = countsat_with_sharpsat (gia, curr_pattern); // no:of input assignments
    else
      assert (false && "Internal tool not supported!");
    undo_solver_assumptions ( solver, result.second );
    patterns.push_back (curr_pattern);
    counts.push_back (curr_count);
    k++;
  }

  // 4. sum all the patterns and the counts.
  mpuint sum = 0;
  assert ( patterns.size() == counts.size() );
  for (auto i=0; i < patterns.size(); i++) {
    sum = sum + ( counts[i] * cirkit::to_multiprecision<mpuint> ( patterns[i] ) );
  }

//#define DEBUG_SOM_PATTERN
#ifdef DEBUG_SOM_PATTERN
  std::cout << "SAT Pattern     :   Count\n";
  for (auto i=0; i < patterns.size(); i++) {
    std::cout << patterns[i] << "    :    " << counts[i] << "\n";
  }
  std::cout << "\n";
#endif
  
  return sum;
  
}


}




// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
