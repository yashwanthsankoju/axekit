// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_error_metrics.hpp
// @brief  : functions to report error metrics using gia.
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef GIA_ERROR_METRICS_HPP
#define GIA_ERROR_METRICS_HPP

#include <boost/dynamic_bitset.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/range/algorithm.hpp>
#include <cstdio>

#define LIN64
#include <functions/gia/gia_arithmetic.hpp>
#include <functions/gia/gia_logical.hpp>
#include <functions/gia/gia_countsat.hpp>
#include <functions/gia/gia_lexsat.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>
#include <functions/cirkit_routines.hpp>

#include <cirkit/bdd_utils.hpp>
#include <cirkit/range_utils.hpp>
#include <cirkit/aig_from_bdd.hpp>

#include <ext-libs/abc/functions/gia_to_cirkit.hpp>
#include <ext-libs/cms5/scalmc.h>

#include <utils/cnf_utils.hpp>

// #include <ext-libs/sharpsat/sharpsat.hpp>

namespace axekit {

using mpuint  = boost::multiprecision::uint256_t;
using mpfloat = boost::multiprecision::cpp_dec_float_100;

// Incorporated solvers : cryptominisat, sharpSAT
// _cnffile versions explicitly writes out a cnffile and this is read into solver.
// without _cnffile solver datatstructures are derived directly.
// _cnffile should behave as the solver is used in the command line.
//
mpuint error_rate ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat );
mpuint error_rate_with_cms ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat );
mpuint error_rate_with_cms_and_cnffile ( abc::Gia_Man_t *fs,
					 abc::Gia_Man_t *fshat );
mpuint error_rate_with_sharpsat ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat );
mpuint error_rate_with_sharpsat_and_cnffile ( abc::Gia_Man_t *fs,
					      abc::Gia_Man_t *fshat );

mpuint worst_case ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat );
mpuint bit_flip_error ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat );

// tool_type =0 for aXc, 1 = for sharpSAT and 2 for CryptoMiniSAT.
// Except sharpSAT, all others are buggy/sideeffects. Hence disabled.
mpfloat average_case ( abc::Gia_Man_t *fs, abc::Gia_Man_t *fshat,
		       const mpuint &evaluated_max_values, const unsigned tool_type = 1u);
//---
//---mpfloat sum_of_errors ( const abc::Gia_Man_t *diff );
//---

}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
