// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_lexsat.cpp
// @brief  : LexSat procedures with Gia
//------------------------------------------------------------------------------


#include "gia_lexsat.hpp"
#include <cstdio>
#include <proof/cec/cec.h>

// More lexsat routines are in gia_countsat.cpp

namespace axekit
{

// Note: There is something strange with the CMS header.
// If this is included, abc::internal solver gives wrong results with double negation.
// Very interesting behaviour. To debug later.
//#include <ext-libs/cms5/scalmc.h>
// Make sure after everything, the CMS headers are not used.
#ifdef CUSP_H_
#error "CUSP/CMS is incompatible with abc::sat_solver_solve_internal()"
#endif


// Notes::
// The way ABC dumps out CNF is
// x1 always omitted for some reason. (can set fReadable to 1 and make this proper)
// x2 onwards outputs.
// and then the gates
// then next is constant 0
// and then the inputs. (last 3 not sure, Need to crosscheck.)
//
// Abc_LitRegular returns a +ve assignment always.
// Abc_Var2Lit (i, 0 is for +ve, and 1 for inversion). Not intuitive, very important.
// 
// Always set the polarity in our case.
//
// LEXSAT procedure. Finds the maximum assignment that can satisfy the network.
//
// TODO: Knuths lexsat procedure. Get an initial assignment after polarity to topup.
boost::dynamic_bitset<> solve_max_lexsat ( abc::Gia_Man_t *gia ) {
  auto num_bits = abc::Gia_ManPoNum (gia);
  auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  auto * solver = (abc::sat_solver *)abc::Cnf_DataWriteIntoSolver( cnf, 1, 0 );
  //char cnffile[] = "max.cnf";
  //abc::Mf_ManDumpCnf ( gia, cnffile, 8, 0, 0, 0 );
  abc::Cnf_DataFree( cnf );

  // set the initial polarity of the solver to max value.
  int polarity[num_bits] = {1};
  std::fill_n (polarity, num_bits, 1);
  //int polarity[num_bits] = { [0 ... num_bits-1] = 1 }; // Only GCC syntax

  boost::dynamic_bitset<> bs (num_bits); // result bitset.

  // 1. prune the const outputs.
  // set polarity for const-0 correctly. correct assumptions on const outputs and result bitset 
  for (auto i=1; i<=num_bits; i++) {
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) )  {
      polarity[i-1] = 0;
      // definite way to get a 0!
      auto ll = abc::Abc_LitNot( abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ); 
      if ( !abc::sat_solver_push (solver, ll) ) { // while pushing itself, its UNSAT
	assert (false && "Const 0 assumption invalidated.");
      }
      bs.set (i-1, false);
    }
    
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) )  {
      // definite way to get a 1!
      auto ll = abc::Abc_LitRegular ( abc::Abc_Var2Lit (i, 0) ) ; 
      if ( !abc::sat_solver_push (solver, ll) ) { // while pushing itself, its UNSAT
	assert (false && "Const 1 assumption invalidated.");
      }
      bs.set (i-1, true);
    }
  }
  
  // 2. set the polarity for the sat-solver
  abc::sat_solver_set_literal_polarity (solver, polarity, num_bits);

  // 3. solve from the outputs.
  for (auto i=bs.size(); i>0 ; i--) { // start from the MSB
    if ( abc::Gia_ManPoIsConst0 (gia, i-1) ) continue; 
    if ( abc::Gia_ManPoIsConst1 (gia, i-1) ) continue; 

    int lit = abc::Abc_Var2Lit (i, 0); // put 1 first.
    bs.set (i-1, true);
    if ( !abc::sat_solver_push (solver, lit) ) { // while pushing itself, its UNSAT
      abc::sat_solver_pop (solver); // revert the last literal.
      bs.set (i-1, false); 
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	abc::sat_solver_pop (solver);
    	assert (false && "inversion cannot be UNSAT [1]");
      }
      continue;  // no need to solve further.
    }
    
    auto status = abc::sat_solver_solve_internal (solver);
    
    if (status == abc::l_True) continue; // assumption holds. Check nxt bit.
    if ( status == abc::l_False ) { // assumption does not hold. Revert and push the opposite
      abc::sat_solver_pop (solver); // revert the last literal.
      lit = abc::Abc_LitNot( abc::Abc_LitRegular( lit ) ); // put 0.
      if ( !abc::sat_solver_push (solver, lit) ) { // inversion cannot be UNSAT
    	assert (false && "inversion cannot be UNSAT [2]");
      }
      bs.set (i-1, false); 
    }
    else {
      assert (false && "Solver status undef");
    }

  }

  sat_solver_delete (solver);
  return bs;
}



}



namespace xyz
{
//
// The below one is the ABC incremental LEXSAT abc::sat_solver_solve_lexsat() in satSolver.c
// Has some problems with our usecase. Need to debug later.
// assert( abc::Abc_LitIsCompl(pLits[iLitFail]) ) is failing when used with get_max_value()
//

// This LEXSAT procedure should be called with a set of literals (pLits, nLits),
// which defines both (1) variable order, and (2) assignment to begin search from.
// It retuns the LEXSAT assigment that is the same or larger than the given one.
// (It assumes that there is no smaller assignment than the one given!)
// The resulting assignment is returned in the same set of literals (pLits, nLits).
// It pushes/pops assumptions internally and will undo them before terminating.
int sat_solver_solve_lexsat( abc::sat_solver* s, int * pLits, int nLits )
{
  int i, iLitFail = -1;
  abc::lbool status;
  assert( nLits > 0 );
  // help the SAT solver by setting desirable polarity
  abc::sat_solver_set_literal_polarity( s, pLits, nLits );
  // check if there exists a satisfying assignment
  status = abc::sat_solver_solve_internal( s );
  if ( status != abc::l_True ) // no assignment
    return status;
  // there is at least one satisfying assignment
  assert( status == abc::l_True );
  // find the first mismatching literal
  for ( i = 0; i < nLits; i++ ) {
    if ( pLits[i] != abc::sat_solver_var_literal(s, abc::Abc_Lit2Var(pLits[i])) )
      break;
  }
  
  if ( i == nLits ) // no mismatch - the current assignment is the minimum one!
    return abc::l_True;
  // mismatch happens in literal i
  iLitFail = i;
  // create assumptions up to this literal (as in pLits) - including this literal!
  for ( i = 0; i <= iLitFail; i++ ) {

    if ( !abc::sat_solver_push(s, pLits[i]) ) // can become UNSAT while adding the last assumption
      break;
    
  }

  if ( i < iLitFail + 1 ) // the solver became UNSAT while adding assumptions
  {
    status = abc::l_False;
    
  }
  else // solve under the assumptions
  {
    status = abc::sat_solver_solve_internal( s );
  }
    

  if ( status == abc::l_True )
  {
    // we proved that there is a sat assignment with literal (iLitFail) having polarity as in pLits
    // continue solving recursively
    if ( iLitFail + 1 < nLits )
      status = xyz::sat_solver_solve_lexsat( s, pLits + iLitFail + 1, nLits - iLitFail - 1 );
  }
  else if ( status == abc::l_False )
  {
    // we proved that there is no assignment with iLitFail having polarity as in pLits
    assert( abc::Abc_LitIsCompl(pLits[iLitFail]) ); // literal is 0 
    // (this assert may fail only if there is a sat assignment smaller than one originally given in pLits)
    // now we flip this literal (make it 1), change the last assumption
    // and contiue looking for the 000...0-assignment of other literals
    sat_solver_pop( s );
    pLits[iLitFail] = abc::Abc_LitNot(pLits[iLitFail]);
    if ( !abc::sat_solver_push(s, pLits[iLitFail]) )
      printf( "sat_solver_solve_lexsat(): A satisfying assignment should exist.\n" ); // because we know that the problem is satisfiable
    // update other literals to be 000...0
    for ( i = iLitFail + 1; i < nLits; i++ )
      pLits[i] = abc::Abc_LitNot( abc::Abc_LitRegular(pLits[i]) );
    // continue solving recursively
    if ( iLitFail + 1 < nLits )
      status = xyz::sat_solver_solve_lexsat( s, pLits + iLitFail + 1, nLits - iLitFail - 1 );
    else
      status = abc::l_True;
  }

  // undo the assumptions
  for ( i = iLitFail; i >= 0; i-- )
    abc::sat_solver_pop( s );
  return status;
}


}




// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
