// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
/**
 * @file mc_accum_error_cycles.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 */

#ifndef MC_CYCLES_FOR_MAX_ACCUM_ERROR_HPP
#define MC_CYCLES_FOR_MAX_ACCUM_ERROR_HPP

#include <utils/common_utils.hpp>
#include <functions/maniac/mc_design.hpp>
#include <functions/maniac/mc_make_miter.hpp>
#include <functions/maniac/mc_bmc.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/regex.hpp>

namespace maniac
{

int mc_accum_error_cycles (
  const mc_design &golden, const mc_design &approx,
  const mc_port &port_to_check, unsigned long acc_error,
  unsigned cycle_limit, unsigned signed_outputs,
  std::string work, bool debug, bool verify, bool print_cex);


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
