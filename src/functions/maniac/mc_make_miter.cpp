// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author Arun <arun@informatik.uni-bremen.de>

#include "mc_make_miter.hpp"

namespace maniac
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
//------------------------------------------------------------------------------



void miter_make_instantiation ( std::ofstream &file,
				const mc_design &golden, const mc_design &approx,
				const mc_port &port_to_check,
				const unsigned signed_outputs ) {

  auto golden_netlist_module = "mc_golden";
  auto approx_netlist_module = "mc_approx";
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();
  
  // 5. golden and approx modules instantiation.
  file << "  " << golden_netlist_module << " maniac_golden_inst1"
       << " (" << std::endl;
  file << "         ." << clock << "(clock), " << std::endl
       << "         ." << reset << "(" << reset << ")";
  for (auto &p : input_ports) // as it is connected.
    file << "," << std::endl << "         ." << p.first << "(" << p.first << ")";
  for (auto &p : output_ports) // except port_to_check & oe, all are dangling.
    file << "," << std::endl << "         ." << p.first << "(" << "golden_" + p.first << ")";

  if (oe != "__NA__") {
    file << "," << std::endl << "         ."  // wire the OE if avaialble.
	 << oe << "(" << "golden_" + oe << ")";
  }
  
  file << std::endl << "          );" << std::endl;
  file << std::endl;

  file << "  " << approx_netlist_module << " maniac_approx_inst1"
       << " (" << std::endl;
  file << "         ." << clock << "(clock), " << std::endl
       << "         ." << reset << "(" << reset << ")";
  for (auto &p : input_ports) // as it is connected.
    file << "," << std::endl << "         ." << p.first << "(" << p.first << ")";
  for (auto &p : output_ports) // except port_to_check & oe, all are dangling.
    file << "," << std::endl << "         ." << p.first << "(" << "approx_" + p.first << ")";

  if (oe != "__NA__") {
    file << "," << std::endl << "         ."  // wire the OE if avaialble.
	 << oe << "(" << "approx_" + oe << ")";
  }

  file << std::endl << "         );" << std::endl;
  file << std::endl;


}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void miter_make_max_error_logic_type2 ( std::ofstream &file, const std::string &miter_out,
					const unsigned long &max_error,
					const mc_port &port_to_check,
					const mc_design &golden, const mc_design &approx,
					const unsigned signed_outputs)
{

  auto golden_netlist_module = golden.get_top_module();
  auto approx_netlist_module = approx.get_top_module();
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();

// MAX BITS checked. Error operations shud be bounded within max_bits
  // If this is edited, type of max_error and accum_error also shud change.
  const unsigned maxBitsSupported = 32;
  auto check_width = port_to_check.second;

  if (1u == check_width)   file << "  wire  " << "maniac_max_error;" << std::endl;
  else file << "  wire  [" << check_width - 1 <<":0] maniac_max_error;" << std::endl;
  file << std::endl;
  
    //--The verilog instantiated here::
    //--
    //-- --oe ::  Output enable wires
    //-- --oe :: wire  golden_oe;
    //-- --oe :: wire  approx_oe;
    //--
    //--wire  [N:0] maniac_abs_diff;
    //--wire  [N:0] golden_out_with_oe;
    //--wire  [N:0] approx_out_with_oe;
    //--wire  [2N:0] maniac_max_error;
    //--
    //--assign golden_out_with_oe = golden_out;
    //--assign approx_out_with_oe = approx_out;
    //----oe  :: assign golden_out_with_oe = golden_oe ? golden_out : N'd0;
    //----oe  :: assign approx_out_with_oe = approx_oe ? approx_out : N'd0;
    //--
    //--assign maniac_abs_diff = (golden_out_with_oe > approx_out_with_oe) ?
    //--     (golden_out_with_oe - approx_out_with_oe) :
    //--     (approx_out_with_oe - golden_out_with_oe) ;
    //--
    //--assign maniac_max_error = {2'd0, maniac_abs_diff};
    //--assign miter_error_out = (maniac_max_error >= 4'd1) ? 1 : 0 ;
    //--
    //--golden maniac_golden_inst1 (... );
    //--approx maniac_approx_inst1 ( ... );
  

  // Difference logic.
  auto gldn = "golden_" + port_to_check.first;
  auto appx = "approx_" + port_to_check.first;
  auto gldn_oe = "golden_" + port_to_check.first + "_with_oe";
  auto appx_oe = "approx_" + port_to_check.first + "_with_oe";
  auto oe_gld = "golden_" + oe;    auto oe_app = "approx_" + oe;
  
  if (oe != "__NA__") {
    file << "  assign " << gldn_oe << " = " << oe_gld << " ? "
	 << gldn << "  :  " << check_width << "'d0" << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << oe_app << " ? "
	 << appx << "  :  " << check_width << "'d0" << ";" << std::endl;
  } else {
    file << "  assign " << gldn_oe << " = " << gldn << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << appx << ";" << std::endl;
  }
  if (signed_outputs > 0) {
    file << "  assign maniac_diff = " << gldn_oe << " - " << appx_oe << ";" << std::endl;
    file << "  assign maniac_diff_twos = ~maniac_diff + 1;" << std::endl;
    file << "  assign maniac_abs_diff = maniac_diff[" << port_to_check.second - 1<< "] ? "
	 << "maniac_diff_twos[" << port_to_check.second - 1<< ":0]   :   maniac_diff;" << std::endl;
  } else {
    file << "  assign maniac_abs_diff = (" << gldn_oe
	 << " > " << appx_oe << ") ?"
	 << std::endl
	 << "         (" << gldn_oe << " - " << appx_oe << ") :"
	 << std::endl
	 << "         (" << appx_oe << " - " << gldn_oe << ") ;" << std::endl;
    file << std::endl;
  }
  // Error logic
  file << "  assign maniac_max_error = {" << check_width  << "'d0, "
       <<"maniac_abs_diff};" << std::endl;
  
  file << "  assign " << miter_out << " = (" << "maniac_max_error"
       << " >= " << (check_width << 1) << "'d"
       << max_error << ") ? 1 : 0 ;" << std::endl;
  file << std::endl;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


void miter_make_header( std::ofstream &file, const std::string &module,
			const std::string &miter_out, const mc_design &design,
			const mc_port &port_to_check, const std::string &output_sign,
			const unsigned signed_outputs)
{
  auto reset = design.get_reset();
  auto oe = design.get_oe();
  auto input_ports = design.get_input_ports();
  auto output_ports = design.get_output_ports();
  
  // clock port shud always be "clock". Else ABC has some problem.
  // 1. Write the header and module. TODO: Include date here
  // TODO: rewrite properly with boost::format
  file << "module "<< module << " ( " << miter_out << ", "
       << "clock" << ", " << reset;
  for (auto &p : input_ports) 
    file << ", " << p.first;
  file << " );" << std::endl << std::endl;

  // 2. Write the input, output.
  file << "  output " << miter_out << ";" << std::endl;
  file << "  input  " << "clock" << ";" << std::endl;
  file << "  input  " << reset << ";" << std::endl;
  for (auto &p : input_ports) {
    assert (p.second > 0);
    if (1u == p.second) file << "  input  " << p.first << ";" << std::endl;
    else file << "  input  [" << p.second - 1 << ":0] " << p.first << ";" << std::endl;
  }
  file << std::endl << std::endl;
  
  // 3. Write the wires and regs
  file << "  wire  " << miter_out << ";" << std::endl;
  file << "  wire  " << "clock" << ";" << std::endl;
  file << "  wire  " << reset << ";" << std::endl;
  for (auto &p : input_ports) {
    assert (p.second > 0);
    if (1u == p.second) file << "  wire  " << p.first << ";" << std::endl;
    else file << "  wire  [" << p.second - 1 << ":0] " << p.first << ";" << std::endl;
  }
  file << std::endl << std::endl;
  
  // These are just dangling wires connected to the output ports.
  // Everything dangling except the port_to_check (and oe, dealt later)
  for (auto &p : output_ports) {
    if (1u == p.second) { // for a 1 bit case, output cannot be signed.
      file << "  wire  golden_" << p.first << ";" << std::endl;
      file << "  wire  approx_" << p.first << ";" << std::endl;
    } 
    else {
      file << "  wire  " << output_sign <<   " [" << p.second - 1 << ":0] "
	   << "golden_" << p.first << ";" << std::endl;
      file << "  wire  " << output_sign << " [" << p.second - 1 << ":0] "
	   << "approx_" << p.first << ";" << std::endl;
    }
  }

  // if there is an oe (output enable signal), we need to consider that.
  // create wires for the OE logic. (always single bit)
  if (oe != "__NA__") {
    file << "  // Output enable wires" << std::endl;
    file << "  wire  golden_" << oe << ";" << std::endl;
    file << "  wire  approx_" << oe << ";" << std::endl;
  }
  file << std::endl << std::endl;
    
  // 4. Extra wires created for miter purpose
  if (1u == port_to_check.second) {
    file << "  wire  maniac_abs_diff" << ";" << std::endl;
    file << "  wire  golden_" + port_to_check.first + "_with_oe;";
    file << "  wire  approx_" + port_to_check.first + "_with_oe;";
  }
  else {
    if (signed_outputs > 0)  {
      file << "  wire signed ["<< port_to_check.second - 1 << ":0]  maniac_diff;"
	   << std::endl;
      file << "  wire        ["<< port_to_check.second << ":0]  maniac_diff_twos;"
	   << std::endl;
    }
    file << "  wire  [" << port_to_check.second - 1 << ":0] "
	 << "maniac_abs_diff" << ";" << std::endl;

    file << "  wire  " << output_sign << " [" << port_to_check.second - 1 << ":0] "
	 << "golden_" + port_to_check.first + "_with_oe;" << std::endl;
    file << "  wire  " << output_sign << " [" << port_to_check.second - 1 << ":0] "
	 << "approx_" + port_to_check.first + "_with_oe;" << std::endl;
  }

}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void miter_make_accum_error_rate_logic_type3 ( std::ofstream &file,
					       const std::string &miter_out,
					       const unsigned long &acc_error_rate,
					       const mc_port &port_to_check,
					       const mc_design &golden,
					       const mc_design &approx,
					       const unsigned signed_outputs )
{

  auto golden_netlist_module = golden.get_top_module();
  auto approx_netlist_module = approx.get_top_module();
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();
  auto port_width = port_to_check.second;

  file << "  reg   [" << port_width - 1 << ":0] maniac_acc_er;" << std::endl;
  file << "  wire  [" << port_width - 1 << ":0] maniac_xor;" << std::endl;
  file << std::endl;

  
  //--The verilog instantiated here::
  //-- --oe  :: // Output enable wires
  //-- --oe  :: wire  golden_oe;
  //-- --oe  :: wire  approx_oe;
  //--
  //--wire  [N:0] maniac_abs_diff;
  //--wire  [N:0] golden_out_with_oe;
  //--wire  [N:0] approx_out_with_oe;
  //--wire  [N:0] maniac_xor;
  //--reg   [N:0] maniac_acc_er;
  //--
  //--assign golden_out_with_oe = golden_out;
  //--assign approx_out_with_oe = approx_out;
  //-- --oe  :: assign golden_out_with_oe = golden_oe ? golden_out : N'd0;
  //-- --oe  :: assign approx_out_with_oe = approx_oe ? approx_out : N'd0;
  //--assign maniac_xor = (golden_out_with_oe ^ approx_out_with_oe);
  //--
  //--assign miter_error_out = ( (maniac_acc_er[0] + maniac_acc_er[1] + ... maniac_acc_er[N-1])  >= XX'dYYY ) ? 1 : 0 ;
  //--
  //--// The error accumulator
  //--always @(posedge clock)
  //--begin
  //--   if (rest)
  //--       maniac_acc_er <= XX'd0;
  //--   else 
  //--       maniac_acc_er <= maniac_acc_er | maniac_xor;
  //--end
  //--golden maniac_golden_inst1 ( ... );
  //--approx maniac_approx_inst1 ( ... );
  

  // Difference logic.
  auto gldn = "golden_" + port_to_check.first;
  auto appx = "approx_" + port_to_check.first;
  auto gldn_oe = "golden_" + port_to_check.first + "_with_oe";
  auto appx_oe = "approx_" + port_to_check.first + "_with_oe";
  auto oe_gld = "golden_" + oe;    auto oe_app = "approx_" + oe;
  
  if (oe != "__NA__") {
    file << "  assign " << gldn_oe << " = " << oe_gld << " ? "
	 << gldn << "  :  " << port_width << "'d0" << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << oe_app << " ? "
	 << appx << "  :  " << port_width << "'d0" << ";" << std::endl;
  } else {
    file << "  assign " << gldn_oe << " = " << gldn << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << appx << ";" << std::endl;
  }
  file << "  assign maniac_xor = (" << gldn_oe
       << " ^ " << appx_oe << ");"  << std::endl;
  file << std::endl;

  // Error logic
  file << "  assign " << miter_out << " = ( ( ";
  for (auto i=0u; i < port_width; i++)
  {
    file << "maniac_acc_er [" << i << "] + ";
  }
  file << "maniac_acc_er [" << port_width - 1 << "] )";
  file << " >= " << port_width << "'d" << acc_error_rate << " ) ? 1 : 0 ;" << std::endl;
  file << std::endl;

  file << "  // The error-rate accumulator" << std::endl;
  file << "  always @(posedge clock)" << std::endl
       << "  begin" << std::endl
       << "     if (" << reset << ")" << std::endl
       << "         maniac_acc_er <= " << port_width << "'d0;" << std::endl
       << "     else " << std::endl
       << "         maniac_acc_er <= maniac_acc_er | maniac_xor;" << std::endl;
  file << "   end " << std::endl;
  file << std::endl;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void miter_make_max_error_logic_type4 ( std::ofstream &file, const std::string &miter_out,
					const unsigned long &max_error,
					const mc_port &port_to_check,
					const mc_design &golden, const mc_design &approx,
					const unsigned signed_outputs)
{

  auto golden_netlist_module = golden.get_top_module();
  auto approx_netlist_module = approx.get_top_module();
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();

  // // just put 1 bit extra for error. Actually it is ceil(log2(port_to_check.second))
  auto check_width = port_to_check.second + 1; 


  file << "  wire  [" << (check_width << 1) - 1 <<":0] maniac_max_error;" << std::endl;
  file << "  wire  [" << port_to_check.second - 1 << ":0] maniac_xor;" << std::endl;
  file << std::endl;
  
  //--The verilog instantiated here::
  //--
  //-- --oe ::  Output enable wires
  //-- --oe :: wire  golden_oe;
  //-- --oe :: wire  approx_oe;
  //--
  //--wire  [N:0] maniac_xor;
  //--wire  [N+1:0] maniac_max_error;
  //--
  //--assign golden_out_with_oe = golden_out;
  //--assign approx_out_with_oe = approx_out;
  //----oe  :: assign golden_out_with_oe = golden_oe ? golden_out : N'd0;
  //----oe  :: assign approx_out_with_oe = approx_oe ? approx_out : N'd0;
  //--
  //--assign maniac_xor = (golden_out_with_oe ^ approx_out_with_oe);
  //--
  //--assign maniac_max_error = maniac_xor[0] + maniac_xor[1] + ... + maniac_xor[N-1];
  //--assign miter_error_out = (maniac_max_error >= 4'd1) ? 1 : 0 ;
  //--
  //--golden maniac_golden_inst1 (... );
  //--approx maniac_approx_inst1 ( ... );
  
  
  // Difference logic.
  auto gldn = "golden_" + port_to_check.first;
  auto appx = "approx_" + port_to_check.first;
  auto gldn_oe = "golden_" + port_to_check.first + "_with_oe";
  auto appx_oe = "approx_" + port_to_check.first + "_with_oe";
  auto oe_gld = "golden_" + oe;    auto oe_app = "approx_" + oe;
  
  if (oe != "__NA__") {
    file << "  assign " << gldn_oe << " = " << oe_gld << " ? "
	 << gldn << "  :  " << check_width << "'d0" << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << oe_app << " ? "
	 << appx << "  :  " << check_width << "'d0" << ";" << std::endl;
  } else {
    file << "  assign " << gldn_oe << " = " << gldn << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << appx << ";" << std::endl;
  }
  file << "  assign maniac_xor = (" << gldn_oe
       << " ^ " << appx_oe << ");"  << std::endl;
  file << std::endl;
  
  // Error logic
  file << "  assign maniac_max_error = " ;
  for (auto i=0u; i < port_to_check.second - 1; i++)
  {
    file << " maniac_xor[" << i << "] + ";
  }
  file << " maniac_xor[" << port_to_check.second -1 << "];" << std::endl;
  file << "  assign " << miter_out << " = (" << "maniac_max_error"
       << " >= " << (check_width << 1) << "'d"
       << max_error << ") ? 1 : 0 ;" << std::endl;
  file << std::endl;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void make_max_error_miter_type2 ( const std::string &miter_module,
				  const std::string &filename, const std::string &miter_out,
				  const mc_design &golden, const mc_design &approx,
				  const mc_port &port_to_check,
				  const std::string &output_sign,
				  const unsigned long &max_error, const unsigned signed_outputs)
{

  std::ofstream file;
  file.open (filename);
  file << std::endl << "// MANIAC Miter Circuit for Max Error Type 2 :: "
       << curr_time() << std::endl;
  
  miter_make_header (file, miter_module, miter_out, golden, port_to_check,
		     output_sign, signed_outputs);
  miter_make_max_error_logic_type2 (file, miter_out, max_error, 
				    port_to_check, golden, approx, signed_outputs);
  miter_make_instantiation (file, golden, approx, port_to_check, signed_outputs);

  file << "endmodule // " << miter_module << std::endl;
  file.close();
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void miter_make_accum_error_logic_type1 ( std::ofstream &file, const std::string &miter_out,
					  const unsigned long &acc_error,
					  const mc_port &port_to_check,
					  const mc_design &golden, const mc_design &approx,
					  const unsigned signed_outputs)
{

  auto golden_netlist_module = golden.get_top_module();
  auto approx_netlist_module = approx.get_top_module();
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();

// MAX BITS checked. Error operations shud be bounded within max_bits
  // If this is edited, type of max_error and accum_error also shud change.
  const unsigned maxBitsSupported = 32;

  // width of accum_error = max (2 x width-port_to_check, 2 x width-accum_error)
  // but making it twice, else need to chk 1bit etc.
  auto check_width = port_to_check.second;
  auto max_bits = msb_pos_of_unsigned ( acc_error ); // acc_error must be unsigned.
  if ( max_bits  > check_width) check_width = max_bits;
  file << "  reg   [" << (check_width << 1) - 1
       << ":0] maniac_accumulated_error;" << std::endl;
  file << std::endl;

  
  //--The verilog instantiated here::
  //-- --oe  :: // Output enable wires
  //-- --oe  :: wire  golden_oe;
  //-- --oe  :: wire  approx_oe;
  //--
  //--wire  [N:0] maniac_abs_diff;
  //--wire  [N:0] golden_out_with_oe;
  //--wire  [N:0] approx_out_with_oe;
  //--reg   [XX:0] maniac_accumulated_error;
  //--
  //--assign golden_out_with_oe = golden_out;
  //--assign approx_out_with_oe = approx_out;
  //-- --oe  :: assign golden_out_with_oe = golden_oe ? golden_out : N'd0;
  //-- --oe  :: assign approx_out_with_oe = approx_oe ? approx_out : N'd0;
  //--assign maniac_abs_diff = (golden_out_with_oe > approx_out_with_oe) ?
  //--       (golden_out_with_oe - approx_out_with_oe) :
  //--       (approx_out_with_oe - golden_out_with_oe) ;
  //--
  //--assign miter_error_out = (maniac_accumulated_error >= XX'dYYY) ? 1 : 0 ;
  //--
  //--// The error accumulator
  //--always @(posedge clock)
  //--begin
  //--   if (rest)
  //--       maniac_accumulated_error <= XX'd0;
  //--   else 
  //--       maniac_accumulated_error <= maniac_accumulated_error + {ZZ'd0, maniac_abs_diff};
  //--end
  //--golden maniac_golden_inst1 ( ... );
  //--approx maniac_approx_inst1 ( ... );
  

  // Difference logic.
  auto gldn = "golden_" + port_to_check.first;
  auto appx = "approx_" + port_to_check.first;
  auto gldn_oe = "golden_" + port_to_check.first + "_with_oe";
  auto appx_oe = "approx_" + port_to_check.first + "_with_oe";
  auto oe_gld = "golden_" + oe;    auto oe_app = "approx_" + oe;
  
  if (oe != "__NA__") {
    file << "  assign " << gldn_oe << " = " << oe_gld << " ? "
	 << gldn << "  :  " << check_width << "'d0" << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << oe_app << " ? "
	 << appx << "  :  " << check_width << "'d0" << ";" << std::endl;
  } else {
    file << "  assign " << gldn_oe << " = " << gldn << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << appx << ";" << std::endl;
  }
  if (signed_outputs > 0) {
    file << "  assign maniac_diff = " << gldn_oe << " - " << appx_oe << ";" << std::endl;
    file << "  assign maniac_diff_twos = ~maniac_diff + 1;" << std::endl;
    file << "  assign maniac_abs_diff = maniac_diff[" << port_to_check.second - 1<< "] ? "
	 << "maniac_diff_twos[" << port_to_check.second - 1<< ":0]   :   maniac_diff;" << std::endl;
  } else {
    file << "  assign maniac_abs_diff = (" << gldn_oe
	 << " > " << appx_oe << ") ?"
	 << std::endl
	 << "         (" << gldn_oe << " - " << appx_oe << ") :"
	 << std::endl
	 << "         (" << appx_oe << " - " << gldn_oe << ") ;" << std::endl;
    file << std::endl;
  }
  // Error logic
  file << "  assign " << miter_out << " = (" << "maniac_accumulated_error"
       << " >= " << (check_width << 1) << "'d"
       << acc_error << ") ? 1 : 0 ;" << std::endl;
  file << std::endl;

  file << "  // The error accumulator" << std::endl;
  file << "  always @(posedge clock)" << std::endl
       << "  begin" << std::endl
       << "     if (" << reset << ")" << std::endl
       << "         maniac_accumulated_error <= " << (check_width << 1)
       << "'d0;" << std::endl
       << "     else " << std::endl
       << "         maniac_accumulated_error <= maniac_accumulated_error + {"
       << (check_width << 1) - port_to_check.second
       << "'d0, "    <<"maniac_abs_diff};" << std::endl
       << "  end" << std::endl;
  file << std::endl;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void miter_make_avg_error_logic_type5_2 ( std::ofstream &file, const std::string &miter_out,
					const unsigned long &avg_error,
					const mc_port &port_to_check,
					const mc_design &golden, const mc_design &approx,
					const unsigned signed_outputs)
{
  assert (false); // IMPLEMENTED. BUT NEED TO TEST.

  auto golden_netlist_module = golden.get_top_module();
  auto approx_netlist_module = approx.get_top_module();
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();

// MAX BITS checked. Error operations shud be bounded within max_bits
  // If this is edited, type of max_error and accum_error also shud change.
  const unsigned maxBitsSupported = 32;

  // width of accum_error = max (2 x width-port_to_check, 2 x width-accum_error)
  // but making it twice, else need to chk 1bit etc.
  auto check_width = port_to_check.second;
  auto max_bits = msb_pos_of_unsigned ( avg_error ); // avg_error must be unsigned.
  if ( max_bits  > check_width) check_width = max_bits;
  file << "  reg   [" << (check_width << 1) - 1
       << ":0] maniac_acc_error;" << std::endl;
  file << "  reg   [31:0] maniac_counter;" << std::endl;
  file << std::endl;

  
  //--The verilog instantiated here::
  //-- --oe  :: // Output enable wires
  //-- --oe  :: wire  golden_oe;
  //-- --oe  :: wire  approx_oe;
  //--
  //--wire  [N:0] maniac_abs_diff;
  //--wire  [N:0] golden_out_with_oe;
  //--wire  [N:0] approx_out_with_oe;
  //--reg   [XX:0] maniac_acc_error;
  //--reg   [31:0] maniac_counter;
  //--
  //--assign golden_out_with_oe = golden_out;
  //--assign approx_out_with_oe = approx_out;
  //-- --oe  :: assign golden_out_with_oe = golden_oe ? golden_out : N'd0;
  //-- --oe  :: assign approx_out_with_oe = approx_oe ? approx_out : N'd0;
  //--assign maniac_abs_diff = (golden_out_with_oe > approx_out_with_oe) ?
  //--       (golden_out_with_oe - approx_out_with_oe) :
  //--       (approx_out_with_oe - golden_out_with_oe) ;
  //--
  //--assign miter_error_out = (maniac_counter == 0) ? 0 :
  //--                 ( (maniac_acc_error/maniac_counter >= XX'dYYY) ? 1 : 0  );
  //--
  //--// The error accumulator
  //--always @(posedge clock)
  //--begin
  //--   if (rest)
  //--       maniac_acc_error <= XX'd0;
  //--   else 
  //--       maniac_acc_error <= maniac_acc_error + {ZZ'd0, maniac_abs_diff};
  //--end
  //---always @(posdedge clock)
  //---  begin
  //---  if (rst) maniac_counter <= 32'd0;
  //---  else maniac_counter <= maniac_counter + 1;
  //---  end
  //--golden maniac_golden_inst1 ( ... );
  //--approx maniac_approx_inst1 ( ... );
  

  // Difference logic.
  auto gldn = "golden_" + port_to_check.first;
  auto appx = "approx_" + port_to_check.first;
  auto gldn_oe = "golden_" + port_to_check.first + "_with_oe";
  auto appx_oe = "approx_" + port_to_check.first + "_with_oe";
  auto oe_gld = "golden_" + oe;    auto oe_app = "approx_" + oe;
  
  if (oe != "__NA__") {
    file << "  assign " << gldn_oe << " = " << oe_gld << " ? "
	 << gldn << "  :  " << check_width << "'d0" << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << oe_app << " ? "
	 << appx << "  :  " << check_width << "'d0" << ";" << std::endl;
  } else {
    file << "  assign " << gldn_oe << " = " << gldn << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << appx << ";" << std::endl;
  }
  if (signed_outputs > 0) {
    file << "  assign maniac_diff = " << gldn_oe << " - " << appx_oe << ";" << std::endl;
    file << "  assign maniac_diff_twos = ~maniac_diff + 1;" << std::endl;
    file << "  assign maniac_abs_diff = maniac_diff[" << port_to_check.second - 1<< "] ? "
	 << "maniac_diff_twos[" << port_to_check.second - 1<< ":0]   :   maniac_diff;" << std::endl;
  } else {
    file << "  assign maniac_abs_diff = (" << gldn_oe
	 << " > " << appx_oe << ") ?"
	 << std::endl
	 << "         (" << gldn_oe << " - " << appx_oe << ") :"
	 << std::endl
	 << "         (" << appx_oe << " - " << gldn_oe << ") ;" << std::endl;
    file << std::endl;
  }
  // Error logic
  file << "  assign " << miter_out << " = " << "(maniac_counter == 0) ? 0 : " << std::endl
       << "           ( maniac_acc_error / maniac_counter )"
       << " >= "
       << avg_error << ") ? 1 : 0 ;" << std::endl;
  file << std::endl;

  file << "  // The error accumulator" << std::endl;
  file << "  always @(posedge clock)" << std::endl
       << "  begin" << std::endl
       << "     if (" << reset << ")" << std::endl
       << "         maniac_acc_error <= " << (check_width << 1)
       << "'d0;" << std::endl
       << "     else " << std::endl
       << "         maniac_acc_error <= maniac_acc_error + {"
       << (check_width << 1) - port_to_check.second
       << "'d0, "    <<"maniac_abs_diff};" << std::endl
       << "  end" << std::endl;
  file << std::endl;
  
  file << "  always @(posedge clock)" << std::endl
       << "  begin" << std::endl
       << "     if (" << reset << ")" << std::endl
       << "         maniac_counter <= 32'd0;" << std::endl
       << "     else " << std::endl
       << "         maniac_counter <= maniac_counter + 1;" << std::endl;
  file << std::endl;
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void miter_make_avg_error_logic_type5_0 ( std::ofstream &file, const std::string &miter_out,
					  const unsigned long &avg_error,
					  const mc_port &port_to_check,
					  const mc_design &golden, const mc_design &approx,
					  const unsigned signed_outputs, const unsigned long &clk_cycles)
{
  assert (false); // IMPLEMENTED. BUT NEED TO TEST.

  auto golden_netlist_module = golden.get_top_module();
  auto approx_netlist_module = approx.get_top_module();
  auto reset = golden.get_reset();
  auto oe = golden.get_oe();
  auto input_ports = golden.get_input_ports();
  auto output_ports = golden.get_output_ports();
  auto clock = golden.get_clock();

  // width of accum_error = max (2 x width-port_to_check, 2 x width-accum_error)
  // but making it twice, else need to chk 1bit etc.
  auto check_width = port_to_check.second;
  auto max_bits = msb_pos_of_unsigned ( avg_error ); // avg_error must be unsigned.
  if ( max_bits  > check_width) check_width = max_bits;
  file << "  reg   [" << (check_width << 1) - 1
       << ":0] maniac_acc_error;" << std::endl;
  file << "  reg   [31:0] maniac_counter;" << std::endl;
  file << "  reg   [31:0] maniac_counter_div;" << std::endl;
  file << std::endl;

  auto M = msb_pos_of_unsigned (clk_cycles);
  assert (M < 32); // Guarantee M is less than 32.

  assert (false); // This idea is wrong.
  
  //--The verilog instantiated here::
  //-- --oe  :: // Output enable wires
  //-- --oe  :: wire  golden_oe;
  //-- --oe  :: wire  approx_oe;
  //--
  //--wire  [N:0] maniac_abs_diff;
  //--wire  [N:0] golden_out_with_oe;
  //--wire  [N:0] approx_out_with_oe;
  //--reg   [XX:0] maniac_acc_error, maniac_acc_error_div;
  //--reg   [31:0] maniac_counter;
  //--
  //--assign golden_out_with_oe = golden_out;
  //--assign approx_out_with_oe = approx_out;
  //-- --oe  :: assign golden_out_with_oe = golden_oe ? golden_out : N'd0;
  //-- --oe  :: assign approx_out_with_oe = approx_oe ? approx_out : N'd0;
  //--assign maniac_abs_diff = (golden_out_with_oe > approx_out_with_oe) ?
  //--       (golden_out_with_oe - approx_out_with_oe) :
  //--       (approx_out_with_oe - golden_out_with_oe) ;
  //--
  //--assign maniac_acc_error_div = ( |(maniac_acc_error[M:0]) + (maniac_acc_error >> M) );
  //--assign miter_error_out = (maniac_counter > clk_cycles) ? 0 :
  //--                 ( maniac_acc_error_div >= XX'dYYY) ? 1 : 0  );
  //--
  //--// The error accumulator
  //--always @(posedge clock)
  //--begin
  //--   if (rest)
  //--       maniac_acc_error <= XX'd0;
  //--   else 
  //--       maniac_acc_error <= maniac_acc_error + {ZZ'd0, maniac_abs_diff};
  //--end
  //---always @(posdedge clock)
  //---  begin
  //---  if (rst) maniac_counter <= 32'd0;
  //---  else maniac_counter <= maniac_counter + 1;
  //---  end
  //--golden maniac_golden_inst1 ( ... );
  //--approx maniac_approx_inst1 ( ... );
  

  // Difference logic.
  auto gldn = "golden_" + port_to_check.first;
  auto appx = "approx_" + port_to_check.first;
  auto gldn_oe = "golden_" + port_to_check.first + "_with_oe";
  auto appx_oe = "approx_" + port_to_check.first + "_with_oe";
  auto oe_gld = "golden_" + oe;    auto oe_app = "approx_" + oe;
  
  if (oe != "__NA__") {
    file << "  assign " << gldn_oe << " = " << oe_gld << " ? "
	 << gldn << "  :  " << check_width << "'d0" << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << oe_app << " ? "
	 << appx << "  :  " << check_width << "'d0" << ";" << std::endl;
  } else {
    file << "  assign " << gldn_oe << " = " << gldn << ";" << std::endl;
    file << "  assign " << appx_oe << " = " << appx << ";" << std::endl;
  }
  if (signed_outputs > 0) {
    file << "  assign maniac_diff = " << gldn_oe << " - " << appx_oe << ";" << std::endl;
    file << "  assign maniac_diff_twos = ~maniac_diff + 1;" << std::endl;
    file << "  assign maniac_abs_diff = maniac_diff[" << port_to_check.second - 1<< "] ? "
	 << "maniac_diff_twos[" << port_to_check.second - 1<< ":0]   :   maniac_diff;" << std::endl;
  } else {
    file << "  assign maniac_abs_diff = (" << gldn_oe
	 << " > " << appx_oe << ") ?"
	 << std::endl
	 << "         (" << gldn_oe << " - " << appx_oe << ") :"
	 << std::endl
	 << "         (" << appx_oe << " - " << gldn_oe << ") ;" << std::endl;
    file << std::endl;
  }
  // Error logic
  file << "  assign " << miter_out << " = " << "(maniac_counter == 0) ? 0 : " << std::endl
       << "           ( maniac_acc_error / maniac_counter )"
       << " >= "
       << avg_error << ") ? 1 : 0 ;" << std::endl;
  file << std::endl;

  file << "  // The error accumulator" << std::endl;
  file << "  always @(posedge clock)" << std::endl
       << "  begin" << std::endl
       << "     if (" << reset << ")" << std::endl
       << "         maniac_acc_error <= " << (check_width << 1)
       << "'d0;" << std::endl
       << "     else " << std::endl
       << "         maniac_acc_error <= maniac_acc_error + {"
       << (check_width << 1) - port_to_check.second
       << "'d0, "    <<"maniac_abs_diff};" << std::endl
       << "  end" << std::endl;
  file << std::endl;
  
  file << "  always @(posedge clock)" << std::endl
       << "  begin" << std::endl
       << "     if (" << reset << ")" << std::endl
       << "         maniac_counter <= 32'd0;" << std::endl
       << "     else " << std::endl
       << "         maniac_counter <= maniac_counter + 1;" << std::endl;
  file << std::endl;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void make_accum_error_miter_type1 ( const std::string &miter_module,
				    const std::string &filename, const std::string &miter_out,
				    const mc_design &golden, const mc_design &approx,
				    const mc_port &port_to_check,
				    const std::string &output_sign,
				    const unsigned long &max_error, const unsigned signed_outputs)
{

  std::ofstream file;
  file.open (filename);
  file << std::endl << "// MANIAC Miter Circuit for Acc Error Type 1 :: "
       << curr_time() << std::endl;
  
  miter_make_header (file, miter_module, miter_out, golden, port_to_check,
		     output_sign, signed_outputs);
  miter_make_accum_error_logic_type1 (file, miter_out, max_error, 
				      port_to_check, golden, approx, signed_outputs);
  miter_make_instantiation (file, golden, approx, port_to_check, signed_outputs);

  file << "endmodule // " << miter_module << std::endl;
  file.close();
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void make_avg_error_miter_type5_2 ( const std::string &miter_module,
				    const std::string &filename, const std::string &miter_out,
				    const mc_design &golden, const mc_design &approx,
				    const mc_port &port_to_check,
				    const std::string &output_sign,
				    const unsigned long &avg_error, const unsigned signed_outputs)
{

  std::ofstream file;
  file.open (filename);
  file << std::endl << "// MANIAC Miter Circuit for Avg Error Type 5_2 (effort 2) :: "
       << curr_time() << std::endl;
  
  miter_make_header (file, miter_module, miter_out, golden, port_to_check,
		     output_sign, signed_outputs);
  miter_make_avg_error_logic_type5_2 (file, miter_out, avg_error, 
				    port_to_check, golden, approx, signed_outputs);
  miter_make_instantiation (file, golden, approx, port_to_check, signed_outputs);

  file << "endmodule // " << miter_module << std::endl;
  file.close();
}
//------------------------------------------------------------------------------
void make_avg_error_miter_type5_1 ( const std::string &miter_module,
				    const std::string &filename, const std::string &miter_out,
				    const mc_design &golden, const mc_design &approx,
				    const mc_port &port_to_check,
				    const std::string &output_sign,
				    const unsigned long &avg_error, const unsigned signed_outputs,
				    const unsigned long &clk_cycles)
{

  assert (false);
}
//------------------------------------------------------------------------------
void make_avg_error_miter_type5_0 ( const std::string &miter_module,
				    const std::string &filename, const std::string &miter_out,
				    const mc_design &golden, const mc_design &approx,
				    const mc_port &port_to_check,
				    const std::string &output_sign,
				    const unsigned long &avg_error, const unsigned signed_outputs,
				    const unsigned long &clk_cycles)
{

  assert (false);
  std::ofstream file;
  file.open (filename);
  file << std::endl << "// MANIAC Miter Circuit for Avg Error Type 5_0 (effort 0):: "
       << curr_time() << std::endl;
  
  miter_make_header (file, miter_module, miter_out, golden, port_to_check,
		     output_sign, signed_outputs);
  miter_make_avg_error_logic_type5_0 (file, miter_out, avg_error, port_to_check,
				      golden, approx, signed_outputs, clk_cycles);
  miter_make_instantiation (file, golden, approx, port_to_check, signed_outputs);

  file << "endmodule // " << miter_module << std::endl;
  file.close();
}
//------------------------------------------------------------------------------
void make_accum_error_rate_miter_type3 ( const std::string &miter_module,
				    const std::string &filename, const std::string &miter_out,
				    const mc_design &golden, const mc_design &approx,
				    const mc_port &port_to_check,
				    const std::string &output_sign,
				    const unsigned long &max_error, const unsigned signed_outputs)
{

  std::ofstream file;
  file.open (filename);
  file << std::endl << "// MANIAC Miter Circuit for Acc Error Rate Type 3 :: "
       << curr_time() << std::endl;
  
  miter_make_header (file, miter_module, miter_out, golden, port_to_check,
		     output_sign, signed_outputs);
  miter_make_accum_error_rate_logic_type3 (file, miter_out, max_error, 
					   port_to_check, golden, approx, signed_outputs);
  miter_make_instantiation (file, golden, approx, port_to_check, signed_outputs);

  file << "endmodule // " << miter_module << std::endl;
  file.close();
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void make_max_error_miter_type4 ( const std::string &miter_module,
				  const std::string &filename, const std::string &miter_out,
				  const mc_design &golden, const mc_design &approx,
				  const mc_port &port_to_check,
				  const std::string &output_sign,
				  const unsigned long &max_error, const unsigned signed_outputs)
{

  std::ofstream file;
  file.open (filename);
  file << std::endl << "// MANIAC Miter Circuit for Max Error Type 2 :: "
       << curr_time() << std::endl;
  
  miter_make_header (file, miter_module, miter_out, golden, port_to_check,
		     output_sign, signed_outputs);
  miter_make_max_error_logic_type4 (file, miter_out, max_error, 
				    port_to_check, golden, approx, signed_outputs);
  miter_make_instantiation (file, golden, approx, port_to_check, signed_outputs);

  file << "endmodule // " << miter_module << std::endl;
  file.close();
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
void mc_make_miter( const std::string &out_filename,
		    const mc_design &golden, const mc_design &approx,
		    std::string miter_module_name, const mc_port &port_to_check,
		    const unsigned long &error_value, const unsigned signed_outputs,
		    const unsigned long &clk_cycles, const mc_miter_type &miter_type) {
  
  std::string output_sign = "";
  if (signed_outputs > 0) output_sign = " signed ";

  if (miter_type == mc_miter_type::type5_0)
  {
    assert (false);
    return;
  }
  if (miter_type == mc_miter_type::type5_1)
  {
    assert (false);
    return;
  }
  // if none of this mc_miter_type fits, pass on.
  mc_make_miter ( out_filename, golden, approx, miter_module_name, port_to_check,
		  error_value, signed_outputs, miter_type );
  
}


void mc_make_miter( const std::string &out_filename,
		    const mc_design &golden, const mc_design &approx,
		    std::string miter_module_name, const mc_port &port_to_check,
		    const unsigned long &error_value, const unsigned signed_outputs,
		    const mc_miter_type &miter_type) {

  std::string output_sign = "";
  if (signed_outputs > 0) output_sign = " signed ";

  if (miter_type == mc_miter_type::type2)
  {
    make_max_error_miter_type2 ( miter_module_name, out_filename, "miter_max_error_out",
				 golden, approx,
				 port_to_check, output_sign, error_value, signed_outputs );
    return;
  }

  if (miter_type == mc_miter_type::type1)
  {
    make_accum_error_miter_type1 ( miter_module_name, out_filename, "miter_accum_error_out",
				   golden, approx,
				   port_to_check, output_sign, error_value, signed_outputs );
    return;
  }

  if (miter_type == mc_miter_type::type4)
  {
    make_max_error_miter_type4 ( miter_module_name, out_filename, "miter_max_error_rate_out",
				 golden, approx,
				 port_to_check, output_sign, error_value, signed_outputs );
    return;
  }

  if (miter_type == mc_miter_type::type3)
  {
    make_accum_error_rate_miter_type3 ( miter_module_name, out_filename,
					"miter_accum_error_rate_out",  golden, approx,
					port_to_check, output_sign, error_value,
					signed_outputs );
    return;
  }

  if (miter_type == mc_miter_type::type5_2)
  {
    make_avg_error_miter_type5_2 ( miter_module_name, out_filename, "miter_avg_error_out",
				   golden, approx,
				   port_to_check, output_sign, error_value, signed_outputs );
    return;
  }

  assert (false); // Should never happen.

}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
