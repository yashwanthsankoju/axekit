// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
/**
 * @file mc_accum_bitflip_cycles.cpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 */

#include "mc_accum_bitflip_cycles.hpp"

namespace maniac
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/


/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

int mc_accum_bitflip_cycles (
  const mc_design &golden, const mc_design &approx,
  const mc_port &port_to_check, unsigned long acc_error_rate,
  unsigned cycle_limit, unsigned signed_outputs,
  std::string work, bool debug, bool verify, bool print_cex)
{
  auto golden_vlog = work + "/golden.v";
  auto approx_vlog = work + "/approx.v";
  golden.write ( golden_vlog, "mc_golden", "verilog" );
  approx.write ( approx_vlog, "mc_approx", "verilog" );

  std::string miter_vlog =  work + "/miter.v";
  std::string miter_blif =  work + "/miter.blif";
  std::string miter_cex  =  work + "/miter.cex";
  std::string miter_summary =  work + "/miter.summary";
  std::string miter_snippet = work + "/m.v";

  // 1. Create the MITER.
  // a. create the miter snippet.
  if (debug) std::cout << "[i] Started  creating accum-error-rate miter :: "
		       << curr_time();
  mc_make_miter ( miter_snippet, golden, approx, "acc_error_rate_miter", port_to_check,
		  acc_error_rate, signed_outputs, mc_miter_type::type3 );
  // b. golden.v + approx.v + miter.v
  axekit::cat_three_files ( miter_vlog, golden_vlog, approx_vlog, miter_snippet );
  auto miter = mc_design ( miter_vlog, "acc_error_rate_miter", "clock",
			   golden.get_reset(), golden.get_oe(), false  );
  miter.write (miter_blif, "blif");
  if (debug) std::cout << "[i] Finished creating accum-error-rate miter :: "
		       << curr_time();

  // 2. Run BMC
  auto cycles = mc_bmc ( miter_blif, miter_cex, miter_summary, cycle_limit, debug, print_cex );
  if (!verify) return cycles;
  if (cycles < 1) return cycles;
  auto new_limit = cycles - 1;
  auto new_result = mc_bmc ( miter_blif, miter_cex, miter_summary, new_limit, debug, false );
  if ( new_result == -1 ) {
    std::cout << "[i] Result Cross Verified. BMC run for cylces " << new_limit
	      << " is UNSAT" << std::endl;
    return cycles;
  }

  std::cout << "[e] Result verified to be FALSE. BMC did not report UNSAT for "
	    << new_limit << " number of cycles " << std::endl;
  return cycles;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
