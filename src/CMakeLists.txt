#-------------------------------------------------------------------------------
# Get the current working branch
execute_process(
  COMMAND git rev-parse --abbrev-ref HEAD
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_BRANCH
  OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Get the latest abbreviated commit hash of the working branch
execute_process(
  COMMAND git log -1 --format=%h
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_COMMIT_HASH
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
add_definitions("-DGIT_COMMIT_HASH=${GIT_COMMIT_HASH}")
add_definitions("-DGIT_BRANCH=${GIT_BRANCH}")

#-------------------------------------------------------------------------------
# Configuration
add_definitions(-Wall -Wno-deprecated-declarations -Wno-unused-variable -Wno-unused-but-set-variable -Wno-unused-function -Wno-sign-compare)
add_definitions(-DABC_NAMESPACE=abc)

#-------------------------------------------------------------------------------
set (TOP-DIR ${CMAKE_SOURCE_DIR})

#set (Boost_DEBUG ON)
#set (Boost_DETAILED_FAILURE_MSG ON)

set( USED_Boost_LIBRARIES
  unit_test_framework
  regex filesystem
  graph
  program_options
  system
  timer
  thread
)

find_package (GMP REQUIRED)
#-Debug later---find_package (Boost 1.56.0 QUIET COMPONENTS ${USED_Boost_LIBRARIES} )
find_package (Boost 1.60.0 COMPONENTS ${USED_Boost_LIBRARIES} REQUIRED)
if (Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIR} ${GMP_INCLUDE_DIR} ${CMAKE_SOURCE_DIR}/ext/include)
  link_directories(${Boost_LIBRARY_DIR} ${CMAKE_SOURCE_DIR}/ext/lib)
  message ("[i]:: boost include dir =  ${Boost_INCLUDE_DIR}")
  message ("[i]:: boost link    dir =  ${Boost_LIBRARY_DIR}")
else ()
  message ("Warning:: Boost LIbs NOT found. Build external first")
endif ()


#-------------------------------------------------------------------------------
# Add NEW libraries here.
new_library (
  NAME
     axekit_all
  LIB_DIRS
     ${TOP-DIR}/src
     ${TOP-DIR}/build/ext/abc-prefix/src/abc/src
  EXTRA_LIBS
     ${Boost_REGEX_LIBRARIES}
     ${Boost_TIMER_LIBRARIES}
     ${Boost_SYSTEM_LIBRARIES}
     ${Boost_FILESYSTEM_LIBRARIES}
     ${Boost_PROGRAM_OPTIONS_LIBRARIES}
     ${GMPXX_LIBRARIES}
     ${GMP_LIBRARIES}
  INCLUDE
  PUBLIC ${CMAKE_BINARY_DIR}/ext/abc-prefix/src/abc/src ${TOP-DIR}/build/ext/sharpSAT-prefix/src
  )

#-------------------------------------------------------------------------------
# Add NEW programs here.
new_program (
  NAME axc
  LIBS axekit_all ${yosys_LIBRARIES} ${abc_LIBRARIES} ${CUDD_LIBRARIES} ${crypto_LIBRARIES} ${sharpSAT_LIBRARIES}
  )

#-------------------------------------------------------------------------------
