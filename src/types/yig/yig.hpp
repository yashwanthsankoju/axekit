// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yig.hpp
// @brief  : The Y inverter Gate. Base type
//
/*-------------------------------------------------------------------------------
Y3::
      a
     /  \
    /    \
   x ----- y
  / \     / \
 /   \   /   \
b----- z ----- c       


Y3  = Maj (  Maj(a, x, y), Maj(x, b, z), Maj(y, z, c) )
Y2b = Maj (a, x, y)  [b = None, z = None, c = None]
Y2r = Maj (x, b, z)  [a = None, y = None, c = None]
Y2l = Maj (y, z, c)  [a = None, x = None, b = None]


Unit clauses and consts
Y(x=y=z)   = x
Y(x=y=z=0) = 0
Y(x=y=z=1) = 1

---------------------------------
Y2:: this is just a Majority Gate.
Y2 is constructed from Y3 using node "none"
      a
     /  \
    /    \
   x ----- y

Y2b = Y3(b=z=c)      = <a, x, y>       (3 i/p MAJ)
Y2b = Y3(b=z=c, y=0) = a.x             (2 i/p AND)
Y2b = Y3(b=z=c, y=1) = a + x           (2 i/p OR)


-------------------------------------------------------------------------------*/
//------------------------------------------------------------------------------

#pragma once
#ifndef YIG_HPP
#define YIG_HPP

#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include <cassert>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graphviz.hpp>

#include <boost/assign/std/vector.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/format.hpp>

#include <cirkit/graph_utils.hpp>

using namespace boost::assign;
using namespace cirkit;

namespace axekit
{

namespace Yig {
using yig_traits_t = boost::adjacency_list_traits <boost::vecS, boost::vecS, boost::directedS>;
using yig_vertex_t = yig_traits_t::vertex_descriptor;

struct Yig_t {
  yig_vertex_t  node;
  bool          complemented;

  inline Yig_t operator!() const {
    return {node, !complemented};
  }

  inline bool operator==( const Yig_t& other ) const {
    return node == other.node && complemented == other.complemented;
  }

  inline bool operator!=( const Yig_t& other ) const {
    return node != other.node || complemented != other.complemented;
  }

  inline bool operator<( const Yig_t& other ) const { // Needed for the sort.
    return node < other.node ||
		  ( node == other.node && complemented < other.complemented );
  }

  inline Yig_t operator^( bool value ) const { // ?
    return {node, complemented != value };
  }

};

// pass to the graph as graph property::graph_name
struct YigGraph_info {
  std::string                                     model_name;
  yig_vertex_t                                    constant;
  yig_vertex_t                                    none;
  bool                                            constant_used = false;
  std::map <yig_vertex_t, std::string>            node_names; // Only to store PI
  std::vector <std::pair<Yig_t, std::string> >    outputs;
  std::vector <yig_vertex_t>                      inputs;

  // As first step for optimization consider smart pointers here.
  // http://stackoverflow.com/questions/10333854/how-to-handle-a-map-with-pointers
  std::map <std::tuple<Yig_t, Yig_t, Yig_t, Yig_t, Yig_t, Yig_t>, Yig_t> strash;
};

using yig_vertex_properties_t = boost::no_property;
using yig_edge_properties_t   = boost::property <boost::edge_complement_t, bool>;
using yig_graph_properties_t  = boost::property <boost::graph_name_t, YigGraph_info>;

using YigGraph_t = boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS,
					   yig_vertex_properties_t,
					   yig_edge_properties_t,
					   yig_graph_properties_t >;

using yig_node = vertex_t<YigGraph_t>; // whts the diff b/w this one, yig_vertex_t?
using yig_edge = edge_t<YigGraph_t>;   // ?


//----------------------------------------------------------------------------------------
// Functions to construct yig.
//----------------------------------------------------------------------------------------
std::string node_name ( const YigGraph_t& yig, const yig_vertex_t &node );
std::string node_name ( const YigGraph_t& yig, const Yig_t &node );

void  start ( YigGraph_t& yig, const std::string& model_name = std::string(),
	      const unsigned size_hint = 0u );
void  stop ();
Yig_t get_constant ( YigGraph_t& yig, bool value );
bool  is_constant_used ( const YigGraph_t& yig );

Yig_t add_yig  ( YigGraph_t& yig, const Yig_t& a, const Yig_t& b, const Yig_t& c,
		 const Yig_t& d,  const Yig_t& e, const Yig_t& f );

inline Yig_t zero ( const YigGraph_t& yig ) { // constant 0
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  auto& info = boost::get_property ( yig, boost::graph_name );
  return { info.constant, false }; // return the constant as it is.
}

inline Yig_t one ( const YigGraph_t& yig ) { // constant 1 
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  auto& info = boost::get_property ( yig, boost::graph_name );
  return { info.constant, true }; // invert the constant.
}

inline Yig_t none ( const YigGraph_t& yig ) {
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  auto& info = boost::get_property ( yig, boost::graph_name );
  return { info.none, false }; // constant doesnt matter
}

inline Yig_t add_pi ( YigGraph_t& yig, const std::string& name ) {
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  yig_node node = add_vertex( yig );

  boost::get_property( yig, boost::graph_name ).inputs += node;
  boost::get_property( yig, boost::graph_name ).node_names[node] = name;

  return { node, false };
}

inline void add_po ( YigGraph_t& yig, const Yig_t& f, const std::string& name ) {
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  boost::get_property( yig, boost::graph_name ).outputs += std::make_pair( f, name );
  std::cout << " Yig: added po " << node_name (yig, f) << " = " << name << "\n";
}

inline Yig_t add_maj ( YigGraph_t& yig, const Yig_t& a, const Yig_t& b, const Yig_t& c ) {
  //std::cout << "in add_maj \n";
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );
  /* Special cases */
  if ( a == b )  { return a; }
  if ( a == c )  { return a; }
  if ( b == c )  { return b; }
  if ( a == !b ) { return c; }
  if ( a == !c ) { return b; }
  if ( b == !c ) { return a; }

  // Order:: a, x, y, b, z, c :  b=none, z=none, c=none
  auto nil = none (yig);
  return add_yig ( yig, a, b, c, nil, nil, nil );
}

// Use MIG style. Some inline checking is already there.
inline Yig_t add_and ( YigGraph_t& yig, const Yig_t& a, const Yig_t& b ) {
  //std::cout << "in add_and \n";
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );
  const auto &zz = zero (yig);
  const auto &nn = one (yig);
  if ( a == b ) return a;
  if ( a == nn ) return b;
  if ( b == nn ) return a;
  if ( a == zz || b == zz ) return nn;
  return add_maj ( yig, zero (yig), a, b );
}

inline Yig_t add_or( YigGraph_t& yig, const Yig_t& a, const Yig_t& b ) {
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );
  const auto &zz = zero (yig);
  const auto &nn = one (yig);
  if ( a == b ) return a;
  if ( a == zz ) return b;
  if ( b == zz ) return a;
  if ( a == nn || b == nn ) return nn;
  return add_maj( yig, one (yig), a, b );
}

inline Yig_t add_xor( YigGraph_t& yig, const Yig_t& a, const Yig_t& b ) {
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );
  const auto &zz = zero (yig);
  const auto &nn = one (yig);
  if ( a == nn ) return !b;
  if ( b == nn ) return !a;
  if ( a == zz ) return b;
  if ( b == zz ) return a;
  if ( a == b  ) return zz;
  if ( a == !b ) return nn;
  
  return add_maj( yig, !a, add_or( yig, a, b ), add_and( yig, a, !b ) );
}

// 3 i/p AND
inline Yig_t add_and ( YigGraph_t& yig, const Yig_t& a, const Yig_t& b, const Yig_t& c ) {
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );
  const auto &zz = zero (yig);
  const auto &nn = one (yig);
  if ( a == zz || b == zz || c == zz ) return zz;
  if ( a == b && b == c  ) return a;
  if ( a == b && b == nn ) return c;
  if ( c == b && b == nn ) return a;
  if ( c == a && a == nn ) return b;
  // Order: a,x,y, b,z,c
  return add_yig ( yig, zz, a, b, zz, c, zz );
}

// 3 i/p OR
inline Yig_t add_or ( YigGraph_t& yig, const Yig_t& a, const Yig_t& b, const Yig_t& c ) {
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );
  const auto &zz = zero (yig);
  const auto &nn = one (yig);
  if ( a == nn || b == nn || c == nn ) return nn;
  if ( a == b && b == c  ) return a;
  if ( a == b && b == zz ) return c;
  if ( c == b && b == zz ) return a;
  if ( c == a && a == zz ) return b;
  // Order: a,x,y, b,z,c
  return add_yig ( yig, nn, a, b, nn, c, nn );
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
inline yig_vertex_t node_to_vertex ( const Yig_t &node) {
  return node.node;
}

inline Yig_t vertex_to_node ( const yig_vertex_t &node, bool complement ) {
  Yig_t yy = {node, complement};
  return yy;
}

inline std::vector<yig_vertex_t> all_pi_nodes ( const YigGraph_t &yig ) {
  return boost::get_property (yig, boost::graph_name).inputs;
}

inline std::vector <std::pair<Yig_t, std::string> > all_pos ( const YigGraph_t &yig ) {
  return boost::get_property (yig, boost::graph_name).outputs;
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

inline Yig_t edge_to_yig ( const YigGraph_t& yig, const yig_edge& edge ) {
  return { target(edge, yig), boost::get( boost::edge_complement, yig )[ edge ] };
}

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
// Read and write yig
void write ( const std::string filename, const YigGraph_t &yig );
void write ( std::ostream &os, const YigGraph_t &yig );

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------


} // Yig namespace

} // axekit namespace

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
