// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yig.hpp
// @brief  : The Y inverter graph. Base type
//
//------------------------------------------------------------------------------

#include "yig.hpp"
#include <cassert>
#include <string>

namespace axekit
{

namespace Yig 
{

//----------------------------------------------------------------------------------------
void start ( YigGraph_t& yig, const std::string& model_name, const unsigned size_hint ) {
  assert( num_vertices( yig ) == 0u );
  if ( size_hint > 0 ) //   // https://svn.boost.org/trac/boost/ticket/1980
    yig.m_vertices.reserve (size_hint); // reserve memory in advance.
  
  auto& info = boost::get_property ( yig, boost::graph_name );
  info.model_name = model_name;

  /* create constant, none nodes */
  info.constant = add_vertex( yig );
  assert( info.constant == 0u );
  info.none = add_vertex (yig);
}

void stop ( ) {
  assert (false);
}

//----------------------------------------------------------------------------------------

Yig_t get_constant( YigGraph_t& yig, bool value ) {
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  auto& info = boost::get_property ( yig, boost::graph_name );
  info.constant_used = true;
  return { info.constant, value };
}

bool is_constant_used( const YigGraph_t& yig ) {
  assert( num_vertices( yig ) != 0u && "Uninitialized YIG" );
  auto& info = boost::get_property ( yig, boost::graph_name );
  return info.constant_used;
}


/*----------------------------------------------------------------------------------------
      a
     /  \
    /    \
   x ----- y
  / \     / \
 /   \   /   \
b----- z ----- c       


Y3  = Maj (  Maj(a, x, y), Maj(x, b, z), Maj(y, z, c) )
Y2b = Maj (a, x, y)  [b = None, z = None, c = None]
Y2r = Maj (x, b, z)  [a = None, y = None, c = None]
Y2l = Maj (y, z, c)  [a = None, x = None, b = None]


Unit clauses and consts
Y(x=y=z)   = x
Y(x=y=z=0) = 0
Y(x=y=z=1) = 1

Y(a=b=c=0)          = x.y.z           (3 i/p AND)
Y(a=b=c=1)          = x + y + z       (3 i/p OR)
Y2b = Y(b=z=c)      = <a, x, y>       (3 i/p MAJ)
Y2b = Y(b=z=c, y=0) = a.x             (2 i/p AND)
Y2b = Y(b=z=c, y=1) = a + x           (2 i/p OR)

----------------------------------------------------------------------------------------*/
Yig_t inline add_yig_int  ( YigGraph_t &yig,
			    const Yig_t &a, const Yig_t &x, const Yig_t &y,
			    const Yig_t &b, const Yig_t &z, const Yig_t &c ) {
  //std::cout << "in add_yig_int \n";
  // Y2b, Y2r and Y2l all can be rotated without any problem.
  // Sort first to make it unique.
  Yig_t children[] = {a, x, y, b, z, c};
  // Note: when we sort it, the gates always will be (nil, nil, nil, yig, yig, yig)
  // unless we have constants. this is not a real problem, since Y2 is symmetric
  std::sort( children, children + 6 );
  const auto key = std::make_tuple ( children[0], children[1], children[2],
				     children[3], children[4], children[5] );

    
  auto& info = boost::get_property ( yig, boost::graph_name );
  const auto it = info.strash.find ( key );
  if ( it != info.strash.end() ) return it->second;
  
  // Add the new node.
  yig_node node = add_vertex( yig );
  assert( node == num_vertices( yig ) - 1u );


  /*std::cout << "Yig add_yig_int added "
	    << node_name (yig, node) << "(strash key = "
	    << node_name (yig, children[0]) << " "
	    << node_name (yig, children[1]) << " "
	    << node_name (yig, children[2]) << " "
	    << node_name (yig, children[3]) << " "
	    << node_name (yig, children[4]) << " "
	    << node_name (yig, children[5]) << ") \n ";
  */
  
  const auto& complement = boost::get( boost::edge_complement, yig );

  const auto ea = add_edge( node, children[0].node, yig ).first;
  const auto ex = add_edge( node, children[1].node, yig ).first;
  const auto ey = add_edge( node, children[2].node, yig ).first;
  const auto eb = add_edge( node, children[3].node, yig ).first;
  const auto ez = add_edge( node, children[4].node, yig ).first;
  const auto ec = add_edge( node, children[5].node, yig ).first;

  complement[ea] = children[0].complemented;
  complement[ex] = children[1].complemented;
  complement[ey] = children[2].complemented;
  complement[eb] = children[3].complemented;
  complement[ez] = children[4].complemented;
  complement[ec] = children[5].complemented;

  return info.strash[key] = { node, false };
}



Yig_t add_yig ( YigGraph_t &yig, const Yig_t &a, const Yig_t &x, const Yig_t &y,
		const Yig_t &b, const Yig_t &z, const Yig_t &c ) {
  assert( num_vertices( yig ) != 0u && "Unitialized YIG" );

  //std::cout << "in add_yig1 \n";

  //std::cout << "    a = " << node_name (yig, y);
  //std::cout << "    b = " << node_name (yig, b);
  //std::cout << "    c = " << node_name (yig, c);
  //std::cout << "    x = " << node_name (yig, x);
  //std::cout << "    y = " << node_name (yig, y);
  //std::cout << "    z = " << node_name (yig, z);
  //std::cout << "\n";
  
  const auto &nil = none (yig);
  // special cases.
  // consts - 0, 1 and unit clauses.
  // all the below cases will become Maj(<x,x,a> <x,x,b> <x,x,c>)
  if ( (x == y) && (y == z) && (x != nil) )  return x; // same vals in middle 
  if ( (a == x) && (x == b) && (x != nil) )  return a; // same vals in left
  if ( (a == y) && (y == c) && (y != nil) )  return a; // same vals in right
  if ( (b == z) && (z == c) && (z != nil) )  return b; // same vals in bottom
  //std::cout << "in add_yig2 \n";
  
  // if there are none, then rearrange it to b=z=c=None
  if ( a == nil ) {
    if ( y == nil ) {
      assert ( c == nil); // All in right side must be None.
      return add_yig_int ( yig, x, b, z, nil, nil, nil ); // rearrange and call internal.
    }
    if ( x == nil ) {
      assert ( b == nil ); // All in left side must be None.
      return add_yig_int ( yig, y, z, c, nil, nil, nil ); // rearrange and call internal.
    }
    assert (false && "Either left or right should be filled with None");
  }
  if ( b == nil ) {
    assert ( a != nil ); // taken care above.
    assert ( (z == nil) && (c == nil) && "Bottom row should be all none" ); 
    return add_yig_int ( yig, a, x, y, nil, nil, nil ); // rearrange and call internal.
  }

  // b in left side, c in right side: already taken care above.
  if ( b == nil ) assert ( z == nil && c == nil ); // bottom row all shd be None
  assert ( z != nil && c != nil); // bottom row all shd NOT be None

  return add_yig_int (yig, a, x, y, b, z, c);
}
//----------------------------------------------------------------------------------------

void write ( std::ostream &os, const YigGraph_t &yig ) {
  auto& info = boost::get_property ( yig, boost::graph_name );

  // 1. print the nodes
  os << ".i " << info.inputs.size()  << "\n";
  os << ".o " << info.outputs.size() << "\n";
  const auto &node_map = info.strash;
  const auto &nil = none (yig);

  // 2. print the outputs.
  for (auto &po : info.outputs) {
    os << po.second << " = " << node_name ( yig, po.first ) << "\n";
  }

  // 3. print all the nodes.
  // a, x, y, b, z, c
  for (auto &node : node_map) {
    auto in_nodes = node.first;
    auto out = node.second;
    assert ( out != none (yig) ); // output cannot be none.
    os << node_name (yig, out) << " = ";
    auto a = std::get<0> (in_nodes);
    auto x = std::get<1> (in_nodes);
    auto y = std::get<2> (in_nodes);
    auto b = std::get<3> (in_nodes);
    auto z = std::get<4> (in_nodes);
    auto c = std::get<5> (in_nodes);

    if ( a == nil || b == nil || c == nil || x == nil || y == nil || z == nil ) {
      os << "Y2(";
      std::vector <std::string> pins;
      if (a != nil) pins.emplace_back ( node_name (yig, a) );
      if (x != nil) pins.emplace_back ( node_name (yig, x) );
      if (y != nil) pins.emplace_back ( node_name (yig, y) );
      if (b != nil) pins.emplace_back ( node_name (yig, b) );
      if (z != nil) pins.emplace_back ( node_name (yig, z) );
      if (c != nil) pins.emplace_back ( node_name (yig, c) );
      assert ( pins.size() == 3);
      for (auto i=0u; i < pins.size()-1; i++)
	os << pins[i] << ", ";
      os << pins [pins.size() - 1];
      os << ")\n";
    }
    else {
      assert (a !=nil && b != nil && x != nil && y != nil && z != nil);
      os << "Y3("
	  << node_name (yig, a) << ", " << node_name (yig, x) << ", "
	  << node_name (yig, y) << ", " << node_name (yig, b) << ", "
	  << node_name (yig, z) << ", " << node_name (yig, c) << ")\n ";
    }
  }

  os << ".e\n";
  os << "PI: ";
  for (auto &i : info.inputs) {
    os << node_name ( yig, i ) << " ";
  }
  os << "\n";
  os << "PI: ";
  for (auto &i : info.outputs) {
    os << i.second << " ";
  }
  os << "\n";
  os << "Written by aXc on \n";
}


void write ( const std::string filename, const YigGraph_t &yig ) {
  std::ofstream ofs (filename);
  write ( ofs, yig );
}



//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

// if the name is entered while constructing then that is retuned.
// else a "n"+ node_index is returned as the name.
std::string node_name ( const YigGraph_t& yig, const yig_vertex_t &node ) {
  auto& info = boost::get_property ( yig, boost::graph_name );
  auto it = info.node_names.find (node);
  if ( it != info.node_names.end() ) return it->second;
  auto vertex_index_map = boost::get (boost::vertex_index_t(), yig);
  auto vit = vertex_index_map[node];
  return std::string ("n") + std::to_string(vit);
}

std::string node_name ( const YigGraph_t& yig, const Yig_t &node ) {
  if ( node == one  (yig) ) return "1";
  if ( node == zero (yig) ) return "0";
  if ( node == none (yig) ) return "nil";
  auto name = node_name ( yig, node.node );
  if ( node.complemented ) return std::string ("~") + name;
  return name;
}



//----------------------------------------------------------------------------------------

} // namespace Yig
} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
