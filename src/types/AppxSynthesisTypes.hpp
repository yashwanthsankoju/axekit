// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : appxSynthesisTypes.hpp
// @brief  : Enums, Classes and typedefs for appx_synthesis program.
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef APPXSYNTHESISTYPES_HPP
#define APPXSYNTHESISTYPES_HPP

#include <types/AbcTypes.hpp>

namespace axekit {
using CutVolume = std::pair < Object, int >; // CutVolume is < root-node, volume >;


enum class SolverResult {
  YES = abc::ProveResult::NOT_EQUIVALENT, // has_limit_crossed() == yes
  NO = abc::ProveResult::EQUIVALENT,      // has_limit_crossed() == no
  ERROR = abc::ProveResult::ERROR,        // has_limit_crossed() == error
  TIMEOUT = abc::ProveResult::UNDECIDED,  // has_limit_crossed() == timeout
  UNINIT = -5                             // Uninitalized, not yet used. MUST GIVE A NUMBER HERE.
};

enum class ErrorType : char {
  WORST_CASE                   // Currently only onetype, worst-case error
};

inline SolverResult ProveResult_to_SolverResult ( const abc::ProveResult &result ) {
  if ( result == abc::ProveResult::EQUIVALENT ) return SolverResult::NO;
  if ( result == abc::ProveResult::NOT_EQUIVALENT ) return SolverResult::YES;
  if ( result == abc::ProveResult::ERROR ) return SolverResult::ERROR;
  if ( result == abc::ProveResult::UNDECIDED ) return SolverResult::TIMEOUT;
  return SolverResult::UNINIT;
}

}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


