// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : appx_synthesis.hpp
//------------------------------------------------------------------------------
#pragma once
#ifndef APPX_SYNTHESIS_HPP
#define APPX_SYNTHESIS_HPP

#include <iostream>
#include <fstream>
#include <utility>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <algorithm>
#include <iomanip>

#include <boost/multiprecision/cpp_int.hpp>

#include <utils/program_options.hpp>
#include <utils/common_utils.hpp>
#include <utils/yosys_utils.hpp>
#include <utils/miter_utils.hpp>
#include <utils/abc_utils.hpp>

#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>

#include <cirkit/cudd_cirkit.hpp>

#include <functions/has_limit_crossed.hpp>
#include <functions/path_routines.hpp>
#include <functions/cut_routines.hpp>
#include <functions/appx_miter.hpp>
#include <functions/cirkit_routines.hpp>

namespace axekit {



}


#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
