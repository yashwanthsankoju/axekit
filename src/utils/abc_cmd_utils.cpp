// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : abc_cmd_utils.cpp
// @brief  : Some utilities that use ABC command line arguments.
//------------------------------------------------------------------------------

#include "abc_cmd_utils.hpp"

namespace axekit
{
//------------------------------------------------------------------------------
void run_abc_cmd (abc::Abc_Frame_t *frame, std::string cmd, bool debug) {
  if (debug) std::cout << "[i] Abc :: " << cmd << std::endl;
  auto status = abc::Cmd_CommandExecute ( frame, cmd.c_str() );
  // Interpretation of status is wrong. Its not a boolean for true or false.
  //if (debug && !status)  std::cout << "[e] Abc :: command " << cmd
  //				   << " failed " << std::endl;
}
//------------------------------------------------------------------------------
// Optimization step resyn2 in ABC - b, rw, rf, b, rw, rwz, b, rfz, rwz, b 
void abc_optimize (abc::Abc_Frame_t *frame) {
  std::cout << "Look here assert (false); // Need an abc based assertion." << std::endl;
  auto localdebug = false;
  run_abc_cmd (frame, "strash", localdebug);
  run_abc_cmd (frame, "balance", localdebug);
  run_abc_cmd (frame, "rewrite", localdebug);
  run_abc_cmd (frame, "refactor", localdebug);
  run_abc_cmd (frame, "balance", localdebug);
  run_abc_cmd (frame, "rewrite", localdebug);
  run_abc_cmd (frame, "rewrite -z", localdebug);
  run_abc_cmd (frame, "balance", localdebug);
  run_abc_cmd (frame, "refactor -z", localdebug);
  run_abc_cmd (frame, "rewrite -z", localdebug);
  run_abc_cmd (frame, "balance", localdebug);
}
//------------------------------------------------------------------------------

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
