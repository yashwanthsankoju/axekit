// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yosys_cmd_utils.cpp
// @brief  : Utilities using Yosys library. All these uses cmd line invokation
// 
// DEPRECIATED. No need to use these.  Rather use yosys_utils.cpp
// 
// TODO: put proper yosys license headers
//------------------------------------------------------------------------------

#include "yosys_cmd_utils.hpp"

namespace axekit
{

//##############################################################################
//
// NOTE: ALL DEPRECIATED ROUTINES. NO NEED TO USE THESE.
//
//##############################################################################


//------------------------------------------------------------------------------
unsigned ys_get_port_width ( const std::string &port_name ) {
  //assert ( is_yosys_started() );
  bool debug = false;
  auto design = Yosys::yosys_get_design();
  auto module = design->top_module();  // get the top module.

  //std::cout << "port_name = " << port_name << std::endl;
  
  for (auto &w : module->wires_) { // _wires is a dict in Module
    auto curr_port = ((w.second)->name).str();
    if ( port_name == Yosys::proper_name (curr_port) ) {
      auto width = (w.second)->width; 
      assert (width > 0 && width < 512); // for ridiculous problems and ports.
      //Yosys::yosys_shutdown(); if shutdown, later re-invokation has problems.
      Yosys::run_yosys_cmd("design -reset ", debug);
      return width;
    }
  }
  //Yosys::yosys_shutdown(); if shutdown, later re-invokation has problems.
  Yosys::run_yosys_cmd("design -reset ", debug);
  assert ( false && "[e] Error: Specified port not found in design!\n" );
  return 0u;
}
//------------------------------------------------------------------------------
// Reads a verilog, synthesise, optimize and writeout blif.
// Invokes standalone Yosys.
//
// IMPORTANT NOTE: Yosys will flatten out the bit-vectors in the LSB-MSB order
// while declaring. But this is how its expected in all our flows including 
// cirkit::mc_* routines and is the NORMAL behavior.
// i.e. 
// module (blah); output [2:0] blah; endmodule; 
//       ---YOSYS--->>> 
//       module ( blah[0], blah[1] ); output blah[0]; output blah[1]; endmodule; 
// 

void ys_verilog_to_blif (std::string iverilog, std::string oblif, 
			 std::string top_module, bool keep, bool debug) {
  // TODO: check if input is verilog itself or not.
  //auto debug = false;
  Yosys::yosys_setup(); // initialize yosys
  Yosys::run_yosys_cmd("read_verilog  " + iverilog, debug);
  auto design = Yosys::yosys_get_design();
  if (!Yosys::has_module ( top_module, design )) { // make sure top-module exists.
    std::cout << "[e] " << top_module << " not found in design!" << iverilog << std::endl;
    assert(false);
  }
  Yosys::run_yosys_cmd("hierarchy -check -top " + top_module, debug);
  ys_optimize(debug); // Do one round of opt with Yosys itself.
  Yosys::run_yosys_cmd("write_blif " + oblif, debug);
  //if (shutdown) Yosys::yosys_shutdown(); if shutdown, later re-invokation has problems.
  if (!keep) Yosys::run_yosys_cmd("design -reset ", debug);
}
//------------------------------------------------------------------------------
// Reads one verilog and two blif files, synthesise and writeout combined blif.
// Invokes standalone Yosys.
void ys_mixed_to_blif (std::string iverilog, std::string iblif1, std::string iblif2,
		       std::string oblif, std::string top_module, bool debug) {
  //auto debug = false;
  //Yosys::yosys_setup(); // initialize yosys
  Yosys::run_yosys_cmd("read_blif  " + iblif1, debug);
  std::cout << "dong\n";
  Yosys::run_yosys_cmd("read_blif  " + iblif2, debug);
  //std::cout << "ys_mixed_to_blif 1\n";
  Yosys::run_yosys_cmd("read_verilog  " + iverilog, debug);
  //std::cout << "ys_mixed_to_blif 2\n";
  auto design = Yosys::yosys_get_design();
  if (!Yosys::has_module ( top_module, design )) { // make sure top-module exists.
    std::cout << "[e] " << top_module << " not found in design!" << iverilog << std::endl;
    assert(false);
  }
  Yosys::run_yosys_cmd("hierarchy -check -top " + top_module, debug);
  ys_optimize(debug); // Do one round of opt with Yosys itself.
  Yosys::run_yosys_cmd("write_blif " + oblif, debug);

  //Yosys::yosys_shutdown(); if shutdown, later re-invokation has problems.
  Yosys::run_yosys_cmd("design -reset ", debug);
}
//------------------------------------------------------------------------------
// All the optimization steps in Yosys.
// This will convert a sequential circuit to a flattaned netlist.
// For combinational circuits, an overkill.
// Does NOT invoke yosys, has to be pre-initialized.
void ys_optimize (bool debug) {
  //assert ( is_yosys_started() );

  Yosys::run_yosys_cmd("proc", debug);
  Yosys::run_yosys_cmd("opt", debug);
  // seg-fault with memory() command. some yosys bug.
  // Problem only wit libyosys, otherwise Ok with the standalone yosys.
  Yosys::run_yosys_cmd("memory_dff", debug);
  Yosys::run_yosys_cmd("opt_clean", debug);
  //Yosys::run_yosys_cmd("memory_share", debug); // Buggy in libyosys.
  Yosys::run_yosys_cmd("memory_collect", debug);
  Yosys::run_yosys_cmd("opt_clean", debug);
  Yosys::run_yosys_cmd("memory_map", debug);
  Yosys::run_yosys_cmd("opt_clean", debug);
  Yosys::run_yosys_cmd("flatten", debug);
  Yosys::run_yosys_cmd("clean", debug);
  Yosys::run_yosys_cmd("fsm", debug);
  Yosys::run_yosys_cmd("opt", debug);
  Yosys::run_yosys_cmd("flatten", debug);
  Yosys::run_yosys_cmd("clean", debug);
  Yosys::run_yosys_cmd("techmap", debug);
  Yosys::run_yosys_cmd("clean", debug);
  Yosys::run_yosys_cmd("opt", debug);
  Yosys::run_yosys_cmd("clean", debug);
}
//------------------------------------------------------------------------------
// Reads a Blif and writes out Verilog. Invokes standalone Yosys.
void ys_blif_to_verilog (std::string iblif, std::string overilog, 
			    std::string top_module) {
  auto debug = false;
  Yosys::yosys_setup(); // initialize yosys
  Yosys::run_yosys_cmd("read_blif " + iblif, debug);
  Yosys::run_yosys_cmd("rename -top " + top_module, debug); // Add top-module
  Yosys::run_yosys_cmd("write_verilog " + overilog, debug);
  //Yosys::yosys_shutdown(); if shutdown, later re-invokation has problems.
  Yosys::run_yosys_cmd("desing -reset"); // reset the design and free up.
}
//------------------------------------------------------------------------------

} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
