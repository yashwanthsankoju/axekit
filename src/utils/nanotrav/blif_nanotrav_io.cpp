// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : blif_nanotrav_io.cpp
// @brief  : implementation of the read_blif and write_blif using Cudd nanotrav
//------------------------------------------------------------------------------

#include "blif_nanotrav_io.hpp"


namespace axekit {

using namespace nanotrav;

const auto NTR_VERSION = "Nanotrav Version #0.13, Release date 2015/7/15";
const unsigned BUFLENGTH = 8192;
char    buffer[BUFLENGTH];

NtrOptions * init_bnet_options() {
  NtrOptions	*option;
  
    /* Initialize option structure. */
    option = ALLOC(NtrOptions,1);
    option->initialTime    = 0;
    option->verify         = FALSE;
    option->second         = FALSE;
    option->file1          = NULL;
    option->file2          = NULL;
    option->traverse       = FALSE;
    option->depend         = FALSE;
    option->image          = NTR_IMAGE_MONO;
    option->imageClip      = 1.0;
    option->approx         = NTR_UNDER_APPROX;
    option->threshold      = -1;
    option->from	   = NTR_FROM_NEW;
    option->groupnsps      = NTR_GROUP_NONE;
    option->closure        = FALSE;
    option->closureClip    = 1.0;
    option->envelope       = FALSE;
    option->scc            = FALSE;
    option->maxflow        = FALSE;
    option->shortPath      = NTR_SHORT_NONE;
    option->selectiveTrace = FALSE;
    option->zddtest        = FALSE;
    option->printcover     = FALSE;
    option->sinkfile       = NULL;
    option->partition      = FALSE;
    option->char2vect      = FALSE;
    option->density        = FALSE;
    option->quality        = 1.0;
    option->decomp         = FALSE;
    option->cofest         = FALSE;
    option->clip           = -1.0;
    option->dontcares      = FALSE;
    option->closestCube    = FALSE;
    option->clauses        = FALSE;
    option->noBuild        = FALSE;
    option->stateOnly      = FALSE;
    option->node           = NULL;
    option->locGlob        = BNET_GLOBAL_DD;
    option->progress       = FALSE;
    option->cacheSize      = 32768;
    option->maxMemory      = 0;	/* set automatically */
    option->maxMemHard     = 0; /* don't set */
    option->maxLive        = ~0U; /* very large number */
    option->slots          = CUDD_UNIQUE_SLOTS;
    option->ordering       = PI_PS_FROM_FILE;
    option->orderPiPs      = NULL;
    option->reordering     = CUDD_REORDER_NONE;
    option->autoMethod     = CUDD_REORDER_SIFT;
    option->autoDyn        = 0;
    option->treefile       = NULL;
    option->firstReorder   = DD_FIRST_REORDER;
    option->countDead      = FALSE;
    option->maxGrowth      = 20;
    option->groupcheck     = CUDD_GROUP_CHECK7;
    option->arcviolation   = 10;
    option->symmviolation  = 10;
    option->recomb         = DD_DEFAULT_RECOMB;
    option->nodrop         = TRUE;
    option->signatures     = FALSE;
    option->verb           = 0;
    option->gaOnOff        = 0;
    option->populationSize = 0;	/* use default */
    option->numberXovers   = 0;	/* use default */
    option->bdddump	   = FALSE;
    option->dumpFmt	   = 0;	/* dot */
    option->dumpfile	   = NULL;
    option->store          = -1; /* do not store */
    option->storefile      = NULL;
    option->load           = FALSE;
    option->loadfile       = NULL;
    option->seed           = 1;

    return (option);

} 


// Function can extract names for inputs and outputs.
// But currently these are ignored.
// Check the Bnet_bddDump() in bnet.cpp for retrieving names.
DdNode ** get_output_bdd ( DdManager *dd,   BnetNetwork * network ) {
  DdNode **outputs = nullptr;
  char **onames = nullptr;
  BnetNode *node;

  assert (network != nullptr);
  assert (dd != nullptr);
  
  if (network->nlatches != 0) {
    std::cout << "[e] Latches in Blif not supported for now.\n"; 
  }

  /* Initialize data structures. */
  auto noutputs = network->noutputs; // outputs includes latches.
  //auto noutputs = network->npos; // these are the primary outputs.
  if (noutputs <1 ) {
    std::cout << "[e] Number of outputs = 0!. Bug?\n";
    assert (false);
  }
  
  outputs = ALLOC (DdNode *,noutputs);
  onames  = ALLOC (char *,noutputs);
  if (outputs == nullptr    ||   onames == nullptr) { // error condition
    if (outputs != nullptr) FREE (outputs);
    if (onames  != nullptr) FREE (onames);
    if (node    != nullptr) FREE (node);
    return nullptr; // error 
  }

  
  // get past the latch CO part. Should not enter for combo networks.
  for (auto i = 0; i < network->nlatches; i++) {
    onames[i] = network->latches[i][0];
    if (!st_lookup(network->hash,network->latches[i][0],(void **)&node)) { // error condition
      if (outputs != nullptr) FREE (outputs);
      if (onames  != nullptr) FREE (onames);
      if (node    != nullptr) FREE (node);
      return nullptr; 
     }
    outputs[i] = node->dd;
  }

  
  for (auto i = 0; i < network->npos; i++) {
    onames[i + network->nlatches] = network->outputs[i];
    if (!st_lookup(network->hash,network->outputs[i],(void **)&node)) { // error condition
      if (outputs != nullptr) FREE (outputs);
      if (onames  != nullptr) FREE (onames);
      if (node    != nullptr) FREE (node);
      return nullptr;
    }
    outputs[i + network->nlatches] = node->dd;
  }

  return outputs;
}


// returns the output bdds from BNetwork and option
DdNode ** get_output_bdd ( NtrOptions* &option,  BnetNetwork* const &net1 ) {
  BnetNode *node;
  
  if (option->node != nullptr) {
    if ( !st_lookup(net1->hash, option->node, (void **)&node) ) {
      std::cout << "[e] No output BDD found in the nanotrav::BnetNetwork() \n";
      return nullptr;
    }
    return &(node->dd);
  }
  else {
    assert (false && "Unknown error: Network contains latches?");
    return nullptr;
  }

  assert (false && "[e] Unknown error!");
  return nullptr; // should not reach here.
}


bool write_nanotrav_blif ( const std::string &file, const cirkit::bdd_function_t &bddf,
			   const std::string &top_module ) {
  auto dd = bddf.first.getManager();
  assert (dd != nullptr);
  
  const auto noutputs = bddf.second.size();
  const auto ninputs  = Cudd_ReadSize(dd);
  if (noutputs < 1) {
    std::cout << "[e] Number of output functions is 0!\n";
    return false;
  }
  if (ninputs < 1) {
    std::cout << "[e] Number of input variables is 0!\n";
    return false;
  }

  DdNode **outputs = ALLOC ( DdNode *, noutputs );
  char **inames    = ALLOC ( char *, Cudd_ReadSize(dd) );
  char **onames    = ALLOC ( char *, noutputs );
  
  FILE *fp = fopen (file.c_str(), "w");

  for (auto i=0; i<noutputs; i++) {  // allot temporary output names.
    std::string po = "f" + std::to_string (i);
    onames[i] = strdup ( po.c_str() );
  }
  for (auto i=0; i<ninputs; i++) { // allot temporary input names.
    std::string pi = "i" + std::to_string (i);
    inames[i] = strdup ( pi.c_str() );
  }

  // get the output nodes.
  for ( auto i=0; i<bddf.second.size(); i++ ) {
    auto bdd = bddf.second[i];
    //outputs[i] = &( Cudd_Regular (bdd.getNode()) );
    outputs[i] = Cudd_Regular (bdd.getNode());
  }
  
  auto res = Cudd_DumpBlif ( dd, noutputs, outputs,
			     (char const * const *) inames,
			     (char const * const *) onames,
			     (char *) top_module.c_str(), fp, 0 );

  if (res) return true;
  return false;

}


// Below function is useless, since we do not maintain the BnetNetwork!
bool write_nanotrav_blif ( const std::string &file, const cirkit::bdd_function_t &bddf,
			   BnetNetwork* const &network ) {
  FILE *fp = fopen (file.c_str(), "w");
  auto cudd = bddf.first;
  auto mgr = cudd.getManager();
  assert (mgr != nullptr);


  int noutputs;
  DdNode **outputs = nullptr;
  char **inames = nullptr;
  char **onames = nullptr;
  BnetNode *node;
    
  if (network->noutputs != network->npos) {
    std::cout << "[w] Design has latches. Cross check the results!\n";
  }

  noutputs = network->noutputs; // includes primary outputs + latches.
  outputs = ALLOC(DdNode *,noutputs);
  onames = ALLOC(char *,noutputs);
  inames = ALLOC(char *,Cudd_ReadSize(mgr));
    
  if (outputs == nullptr ) { // error
    std::cout << "[e] Number of outputs in the design is 0\n";
    if (outputs != nullptr) FREE (outputs);
    if (onames  != nullptr) FREE (onames);
    if (inames  != nullptr) FREE (inames);
    if (node    != nullptr) FREE (node);
    fclose (fp);
    return false;
  }

  // 1. get latches. Should not print anything in combo circuits.
  for (auto i = 0; i < network->nlatches; i++) {
    onames[i] = network->latches[i][0];
    if (!st_lookup(network->hash,network->latches[i][0],(void **)&node)) { // error
      if (outputs != nullptr) FREE (outputs);
      if (onames  != nullptr) FREE (onames);
      if (inames  != nullptr) FREE (inames);
      if (node    != nullptr) FREE (node);
      fclose (fp);
      return false;
    }
    outputs[i] = node->dd;
  }

  // 2. get PO.
  for (auto i = 0; i < network->npos; i++) {
    onames[i + network->nlatches] = network->outputs[i];
    if (!st_lookup(network->hash,network->outputs[i],(void **)&node)) { // error
      if (outputs != nullptr) FREE (outputs);
      if (onames  != nullptr) FREE (onames);
      if (inames  != nullptr) FREE (inames);
      if (node    != nullptr) FREE (node);
      fclose (fp);
      return false;
    }
    outputs[i + network->nlatches] = node->dd;
  }
  
  // 3. get PI.
  for (auto i = 0; i < network->ninputs; i++) {
    if (!st_lookup(network->hash,network->inputs[i],(void **)&node)) { // error
      if (outputs != nullptr) FREE (outputs);
      if (onames  != nullptr) FREE (onames);
      if (inames  != nullptr) FREE (inames);
      if (node    != nullptr) FREE (node);
      fclose (fp);
      return false;
    }
    inames[node->var] = network->inputs[i];
  }

  // 4. latches as PI.
  for (auto i = 0; i < network->nlatches; i++) {
    if (!st_lookup(network->hash,network->latches[i][1],(void **)&node)) { // error
      if (outputs != nullptr) FREE (outputs);
      if (onames  != nullptr) FREE (onames);
      if (inames  != nullptr) FREE (inames);
      if (node    != nullptr) FREE (node);
      fclose (fp);
      return false;
    }
    inames[node->var] = network->latches[i][1];
  }
  
  auto res = Cudd_DumpBlif ( mgr, noutputs, outputs,
			     (char const * const *) inames,
			     (char const * const *) onames,
			     network->name, fp, 0 );
  
  
  if (outputs != nullptr) FREE (outputs);
  if (onames  != nullptr) FREE (onames);
  if (inames  != nullptr) FREE (inames);
  if (node    != nullptr) FREE (node);
  fclose (fp);
  return true;
    
}

// does not preserve the names.. :( TODO: get this later.
cirkit::bdd_function_t read_nanotrav_blif (const std::string &file, Cudd &cudd) {
  FILE *fp = fopen (file.c_str(), "r");
  cirkit::bdd_function_t bdd;
  
  auto ntk = Bnet_ReadNetwork (fp);
  if (ntk == nullptr) {
    std::cout << "[e] Error/Unknown syntax in BLIF flie: " << file << std::endl;
    fclose (fp);
    return { cudd, {cudd.bddZero()} };
  }
  
  auto option = init_bnet_options();
  auto mgr = cudd.getManager();
  assert (mgr != nullptr);
  auto status = Ntr_build_shared_DDs (ntk, mgr, option);
  //auto status = Ntr_buildDDs (ntk, mgr, option, NULL);
  if (0 == status) {
    std::cout << "[e] Error in building BDD. Cannot parse BLIF file\n";
    fclose (fp);
    return {cudd, {cudd.bddZero()}};
  }
  
  std::vector<BDD> bddvec;
  std::cout << "[i] Number of output bits = " << ntk->npos << "\n";
  if (ntk->npos != ntk->noutputs) {
    std::cout << "[e] Latches in the BDD. Output is unusable\n";
  }

  if (option->node != nullptr) { // this is not normally invoked. Donno why.
    std::cout << "[w] Something wrong!. Cross check the results\n";
    auto outnodes = get_output_bdd (option, ntk);
    for (auto i=0; i< (ntk->npos); i++ ) { 
      auto mynode = outnodes [i];
      bddvec.emplace_back ( BDD (cudd, mynode) );
    }
    
    bdd = {cudd, bddvec};
  }
  else { // this should be the normal branch taken.
    auto outnodes = get_output_bdd (mgr, ntk);
    if (outnodes == nullptr) { // error condition.
      bdd = {cudd, {cudd.bddZero()}};
    }
    else {
      for (auto i=0; i< (ntk->npos); i++ ) {  // Normal execution code.
	//auto mynode = Cudd_Regular ( outnodes [i] ); // this is wrong.
	auto mynode = ( outnodes [i] );
	if (mynode == DD_ONE(mgr) ) {
	  std::cout << "[w] Const 1 output function (f" << i << ")\n";
	  /*std::cout << "mynode = " <<  mynode
		    << "   DD_ONE(mgr) = " << DD_ONE(mgr)
		    << "   Cudd_Not (DD_ONE (mgr) ) = " << Cudd_Not (DD_ONE (mgr) )
		    << std::endl; */
	}
	if (mynode == Cudd_Not (DD_ONE (mgr) ) ) {
	  std::cout << "[w] Const 0 output function (f" << i << ")\n";
	  /*std::cout << "mynode = " << mynode
		    << "   DD_ONE(mgr) = " << DD_ONE(mgr)
		    << "   Cudd_Not (DD_ONE (mgr) ) = " << Cudd_Not (DD_ONE (mgr) )
		    << std::endl; */

	}
	bddvec.emplace_back ( BDD (cudd, mynode ) );
      }
      bdd = {cudd, bddvec};
    }
      
  }

  Bnet_FreeNetwork (ntk);
  fclose (fp);
  return bdd;
}



} // namespace axekit

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
