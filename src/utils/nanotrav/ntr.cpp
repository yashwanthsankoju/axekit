/**
  @file

  @ingroup nanotrav

  @brief A very simple reachability analysis program.

  @author Fabio Somenzi

  @copyright@parblock
  Copyright (c) 1995-2015, Regents of the University of Colorado

  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

  Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  Neither the name of the University of Colorado nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
  @endparblock

*/

#include <cuddInt.h>
#include "ntr.hpp"


namespace nanotrav {
/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

#define NTR_MAX_DEP_SIZE 20

/*---------------------------------------------------------------------------*/
/* Stucture declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

static char const *onames[] = {"T", "R"}; /**< names of functions to be dumped */
static double *signatures;	/**< signatures for all variables */
static BnetNetwork *staticNet;	/**< pointer to network used by qsort
				 ** comparison function */
static DdNode **staticPart;	/**< pointer to parts used by qsort
				 ** comparison function */

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/** \cond */

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

static DdNode * makecube (DdManager *dd, DdNode **x, int n);
static void ntrInitializeCount (BnetNetwork *net, NtrOptions *option);
static void ntrCountDFS (BnetNetwork *net, BnetNode *node);
static void ntrIncreaseRef(void * e, void * arg);
static void ntrDecreaseRef(void * e, void * arg);

/** \endcond */


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/


/**
  @brief Builds DDs for a network outputs and next state functions.

  @details The method is really brain-dead, but it is very simple.
  Some inputs to the network may be shared with another network whose
  DDs have already been built.  In this case we want to share the DDs
  as well.

  @return 1 in case of success; 0 otherwise.

  @sideeffect the dd fields of the network nodes are modified. Uses the
  count fields of the nodes.

*/
int
Ntr_buildDDs(
  BnetNetwork * net /**< network for which DDs are to be built */,
  DdManager * dd /**< %DD manager */,
  NtrOptions * option /**< option structure */,
  BnetNetwork * net2 /**< companion network with which inputs may be shared */)
{
    int pr = option->verb;
    int result;
    int i;
    BnetNode *node, *node2;

    /* If some inputs or present state variables are shared with
    ** another network, we initialize their BDDs from that network.
    */
    if (net2 != NULL) {
	for (i = 0; i < net->npis; i++) {
	    if (!st_lookup(net->hash,net->inputs[i],(void **)&node)) {
		return(0);
	    }
	    if (!st_lookup(net2->hash,net->inputs[i],(void **)&node2)) {
		/* This input is not shared. */
		result = Bnet_BuildNodeBDD(dd,node,net->hash,
					   option->locGlob,option->nodrop);
		if (result == 0) return(0);
	    } else {
		if (node2->dd == NULL) return(0);
		node->dd = node2->dd;
		Cudd_Ref(node->dd);
		node->var = node2->var;
		node->active = node2->active;
	    }
	}
	for (i = 0; i < net->nlatches; i++) {
	    if (!st_lookup(net->hash,net->latches[i][1],(void **)&node)) {
		return(0);
	    }
	    if (!st_lookup(net2->hash,net->latches[i][1],(void **)&node2)) {
		/* This present state variable is not shared. */
		result = Bnet_BuildNodeBDD(dd,node,net->hash,
					   option->locGlob,option->nodrop);
		if (result == 0) return(0);
	    } else {
		if (node2->dd == NULL) return(0);
		node->dd = node2->dd;
		Cudd_Ref(node->dd);
		node->var = node2->var;
		node->active = node2->active;
	    }
	}
    } else {
	/* First assign variables to inputs if the order is provided.
	** (Either in the .blif file or in an order file.)
	*/
	if (option->ordering == PI_PS_FROM_FILE) {
	    /* Follow order given in input file. First primary inputs
	    ** and then present state variables.
	    */
	    for (i = 0; i < net->npis; i++) {
		if (!st_lookup(net->hash,net->inputs[i],(void **)&node)) {
		    return(0);
		}
		result = Bnet_BuildNodeBDD(dd,node,net->hash,
					   option->locGlob,option->nodrop);
		if (result == 0) return(0);
	    }
	    for (i = 0; i < net->nlatches; i++) {
		if (!st_lookup(net->hash,net->latches[i][1],(void **)&node)) {
		    return(0);
		}
		result = Bnet_BuildNodeBDD(dd,node,net->hash,
					   option->locGlob,option->nodrop);
		if (result == 0) return(0);
	    }
	} else if (option->ordering == PI_PS_GIVEN) {
	    result = Bnet_ReadOrder(dd,option->orderPiPs,net,option->locGlob,
				    option->nodrop);
	    if (result == 0) return(0);
	} else {
	    result = Bnet_DfsVariableOrder(dd,net);
	    if (result == 0) return(0);
	}
    }
    /* At this point the BDDs of all primary inputs and present state
    ** variables have been built. */

    /* Currently noBuild doesn't do much. */
    if (option->noBuild == TRUE)
	return(1);

    if (option->locGlob == BNET_LOCAL_DD) {
	node = net->nodes;
	while (node != NULL) {
	    result = Bnet_BuildNodeBDD(dd,node,net->hash,BNET_LOCAL_DD,TRUE);
	    if (result == 0) {
		return(0);
	    }
	    if (pr > 2) {
		(void) fprintf(stdout,"%s",node->name);
		Cudd_PrintDebug(dd,node->dd,Cudd_ReadSize(dd),pr);
	    }
	    node = node->next;
	}
    } else { /* option->locGlob == BNET_GLOBAL_DD */
	/* Create BDDs with DFS from the primary outputs and the next
	** state functions. If the inputs had not been ordered yet,
	** this would result in a DFS order for the variables.
	*/

	ntrInitializeCount(net,option);

	if (option->node != NULL &&
	    option->closestCube == FALSE && option->dontcares == FALSE) {
	    if (!st_lookup(net->hash,option->node,(void **)&node)) {
		return(0);
	    }
	    result = Bnet_BuildNodeBDD(dd,node,net->hash,BNET_GLOBAL_DD,
				       option->nodrop);
	    if (result == 0) return(0);
	} else {
	    if (option->stateOnly == FALSE) {
		for (i = 0; i < net->npos; i++) {
		    if (!st_lookup(net->hash,net->outputs[i],(void **)&node)) {
			continue;
		    }
		    result = Bnet_BuildNodeBDD(dd,node,net->hash,
					       BNET_GLOBAL_DD,option->nodrop);
		    if (result == 0) return(0);
		    if (option->progress)  {
			(void) fprintf(stdout,"%s\n",node->name);
		    }
#if 0
		    Cudd_PrintDebug(dd,node->dd,net->ninputs,option->verb);
#endif
		}
	    }
	    for (i = 0; i < net->nlatches; i++) {
		if (!st_lookup(net->hash,net->latches[i][0],(void **)&node)) {
		    continue;
		}
		result = Bnet_BuildNodeBDD(dd,node,net->hash,BNET_GLOBAL_DD,
					   option->nodrop);
		if (result == 0) return(0);
		if (option->progress)  {
		    (void) fprintf(stdout,"%s\n",node->name);
		}
#if 0
		Cudd_PrintDebug(dd,node->dd,net->ninputs,option->verb);
#endif
	    }
	}
	/* Make sure all inputs have a DD and dereference the DDs of
	** the nodes that are not reachable from the outputs.
	*/
	for (i = 0; i < net->npis; i++) {
	    if (!st_lookup(net->hash,net->inputs[i],(void **)&node)) {
		return(0);
	    }
	    result = Bnet_BuildNodeBDD(dd,node,net->hash,BNET_GLOBAL_DD,
				       option->nodrop);
	    if (result == 0) return(0);
	    if (node->count == -1) Cudd_RecursiveDeref(dd,node->dd);
	}
	for (i = 0; i < net->nlatches; i++) {
	    if (!st_lookup(net->hash,net->latches[i][1],(void **)&node)) {
		return(0);
	    }
	    result = Bnet_BuildNodeBDD(dd,node,net->hash,BNET_GLOBAL_DD,
				       option->nodrop);
	    if (result == 0) return(0);
	    if (node->count == -1) Cudd_RecursiveDeref(dd,node->dd);
	}

	/* Dispose of the BDDs of the internal nodes if they have not
	** been dropped already.
	*/
	if (option->nodrop == TRUE) {
	    for (node = net->nodes; node != NULL; node = node->next) {
		if (node->dd != NULL && node->count != -1 &&
		    (node->type == BNET_INTERNAL_NODE ||
		    node->type == BNET_INPUT_NODE ||
		    node->type == BNET_PRESENT_STATE_NODE)) {
		    Cudd_RecursiveDeref(dd,node->dd);
		    if (node->type == BNET_INTERNAL_NODE) node->dd = NULL;
		}
	    }
	}
    }

    return(1);

} /* end of Ntr_buildDDs */





/**
  @brief Builds the %BDD of the initial state(s).

  @return a %BDD if successful; NULL otherwise.

  @sideeffect None

*/
DdNode *
Ntr_initState(
  DdManager * dd,
  BnetNetwork * net,
  NtrOptions * option)
{
    DdNode *res, *x, *w, *one;
    BnetNode *node;
    int i;

    if (option->load) {
      assert (false && "ERROR: Dddmp support not implemented in axekit::nanotrav version");
      //res = Dddmp_cuddBddLoad(dd, DDDMP_VAR_MATCHIDS, NULL, NULL, NULL,
      //                        DDDMP_MODE_DEFAULT, option->loadfile, NULL);
    } else {
	one = Cudd_ReadOne(dd);
	Cudd_Ref(res = one);

	if (net->nlatches == 0) return(res);

	for (i = 0; i < net->nlatches; i++) {
	    if (!st_lookup(net->hash,net->latches[i][1],(void **)&node)) {
		goto endgame;
	    }
	    x = node->dd;
	    switch (net->latches[i][2][0]) {
	    case '0':
		w = Cudd_bddAnd(dd,res,Cudd_Not(x));
		break;
	    case '1':
		w = Cudd_bddAnd(dd,res,x);
		break;
	    default: /* don't care */
		w = res;
		break;
	    }

	    if (w == NULL) {
		Cudd_RecursiveDeref(dd,res);
		return(NULL);
	    }
	    Cudd_Ref(w);
	    Cudd_RecursiveDeref(dd,res);
	    res = w;
	}
    }
    return(res);

endgame:

    return(NULL);

} /* end of Ntr_initState */


/**
  @brief Reads a state cube from a file or creates a random one.

  @return a pointer to the %BDD of the sink nodes if successful; NULL
  otherwise.

  @sideeffect None

*/
DdNode *
Ntr_getStateCube(
  DdManager * dd,
  BnetNetwork * net,
  char * filename,
  int  pr)
{
    FILE *fp;
    DdNode *cube;
    DdNode *w;
    char *state;
    int i;
    int err;
    BnetNode *node;
    DdNode *x;
    char c[2];

    cube = Cudd_ReadOne(dd);
    if (net->nlatches == 0) {
	Cudd_Ref(cube);
	return(cube);
    }

    state = ALLOC(char,net->nlatches+1);
    if (state == NULL)
	return(NULL);
    state[net->nlatches] = 0;

    if (filename == NULL) {
	/* Pick one random minterm. */
	for (i = 0; i < net->nlatches; i++) {
	    state[i] = (char) ((Cudd_Random(dd) & 0x2000) ? '1' : '0');
	}
    } else {
	if ((fp = fopen(filename,"r")) == NULL) {
	    (void) fprintf(stderr,"Unable to open %s\n",filename);
	    return(NULL);
	}

	/* Read string from file. Allow arbitrary amount of white space. */
	for (i = 0; !feof(fp); i++) {
	    err = fscanf(fp, "%1s", c);
	    state[i] = c[0];
	    if (err == EOF || i == net->nlatches - 1) {
		break;
	    } else if (err != 1 || strchr("012xX-", c[0]) == NULL ) {
		FREE(state);
		return(NULL);
	    }
	}
	err = fclose(fp);
	if (err == EOF) {
	    FREE(state);
	    return(NULL);
	}
    }

    /* Echo the chosen state(s). */
    if (pr > 0) {(void) fprintf(stdout,"%s\n", state);}

    Cudd_Ref(cube);
    for (i = 0; i < net->nlatches; i++) {
	if (!st_lookup(net->hash,net->latches[i][1],(void **)&node)) {
	    Cudd_RecursiveDeref(dd,cube);
	    FREE(state);
	    return(NULL);
	}
	x = node->dd;
	switch (state[i]) {
	case '0':
	    w = Cudd_bddAnd(dd,cube,Cudd_Not(x));
	    break;
	case '1':
	    w = Cudd_bddAnd(dd,cube,x);
	    break;
	default: /* don't care */
	    w = cube;
	    break;
	}

	if (w == NULL) {
	    Cudd_RecursiveDeref(dd,cube);
	    FREE(state);
	    return(NULL);
	}
	Cudd_Ref(w);
	Cudd_RecursiveDeref(dd,cube);
	cube = w;
    }

    FREE(state);
    return(cube);

} /* end of Ntr_getStateCube */


/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/


/**
  @brief Builds a positive cube of all the variables in x.

  @return a %BDD for the cube if successful; NULL otherwise.

  @sideeffect None

*/
static DdNode *
makecube(
  DdManager * dd,
  DdNode ** x,
  int  n)
{
    DdNode *res, *w, *one;
    int i;

    one = Cudd_ReadOne(dd);
    Cudd_Ref(res = one);

    for (i = n-1; i >= 0; i--) {
	w = Cudd_bddAnd(dd,res,x[i]);
	if (w == NULL) {
	    Cudd_RecursiveDeref(dd,res);
	    return(NULL);
	}
	Cudd_Ref(w);
	Cudd_RecursiveDeref(dd,res);
	res = w;
    }
    Cudd_Deref(res);
    return(res);

} /* end of makecube */


/**
  @brief Initializes the count fields used to drop DDs.

  @details Before actually building the BDDs, we perform a DFS from
  the outputs to initialize the count fields of the nodes.  The
  initial value of the count field will normally coincide with the
  fanout of the node.  However, if there are nodes with no path to any
  primary output or next state variable, then the initial value of
  count for some nodes will be less than the fanout. For primary
  outputs and next state functions we add 1, so that we will never try
  to free their DDs. The count fields of the nodes that are not
  reachable from the outputs are set to -1.

  @sideeffect Changes the count fields of the network nodes. Uses the
  visited fields.

*/
static void
ntrInitializeCount(
  BnetNetwork * net,
  NtrOptions * option)
{
    BnetNode *node;
    int i;

    if (option->node != NULL &&
	option->closestCube == FALSE && option->dontcares == FALSE) {
	if (!st_lookup(net->hash,option->node,(void **)&node)) {
	    (void) fprintf(stdout, "Warning: node %s not found!\n",
			   option->node);
	} else {
	    ntrCountDFS(net,node);
	    node->count++;
	}
    } else {
	if (option->stateOnly == FALSE) {
	    for (i = 0; i < net->npos; i++) {
		if (!st_lookup(net->hash,net->outputs[i],(void **)&node)) {
		    (void) fprintf(stdout,
				   "Warning: output %s is not driven!\n",
				   net->outputs[i]);
		    continue;
		}
		ntrCountDFS(net,node);
		node->count++;
	    }
	}
	for (i = 0; i < net->nlatches; i++) {
	    if (!st_lookup(net->hash,net->latches[i][0],(void **)&node)) {
		(void) fprintf(stdout,
			       "Warning: latch input %s is not driven!\n",
			       net->outputs[i]);
		continue;
	    }
	    ntrCountDFS(net,node);
	    node->count++;
	}
    }

    /* Clear visited flags. */
    node = net->nodes;
    while (node != NULL) {
	if (node->visited == 0) {
	    node->count = -1;
	} else {
	    node->visited = 0;
	}
	node = node->next;
    }

} /* end of ntrInitializeCount */


/**
  @brief Does a DFS from a node setting the count field.

  @sideeffect Changes the count and visited fields of the nodes it
  visits.

  @see ntrLevelDFS

*/
static void
ntrCountDFS(
  BnetNetwork * net,
  BnetNode * node)
{
    int i;
    BnetNode *auxnd;

    node->count++;

    if (node->visited == 1) {
	return;
    }

    node->visited = 1;

    for (i = 0; i < node->ninp; i++) {
	if (!st_lookup(net->hash, node->inputs[i], (void **)&auxnd)) {
	    exit(2);
	}
	ntrCountDFS(net,auxnd);
    }

} /* end of ntrCountDFS */



/**
  @brief Calls Cudd_Ref on its first argument.
*/
static void
ntrIncreaseRef(
  void * e,
  void * arg)
{
  DdNode * node = (DdNode *) e;
  (void) arg; /* avoid warning */
  Cudd_Ref(node);

} /* end of ntrIncreaseRef */


/**
  @brief Calls Cudd_RecursiveDeref on its first argument.
*/
static void
ntrDecreaseRef(
  void * e,
  void * arg)
{
  DdNode * node = (DdNode *) e;
  DdManager * dd = (DdManager *) arg;
  Cudd_RecursiveDeref(dd, node);

} /* end of ntrIncreaseRef */


} // namespace nanotrav

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

