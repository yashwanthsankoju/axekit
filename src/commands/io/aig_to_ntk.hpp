// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_to_ntk.hpp
// @brief  : converts an AIG store (in Gia_Man_t*) to a Old Network store
//
//------------------------------------------------------------------------------
#pragma once

#ifndef AIG_TO_NTK_HPP
#define AIG_TO_NTK_HPP

#define LIN64

#include <commands/axekit_stores.hpp>

namespace alice {

class aig_to_ntk_command : public command {
public:
  aig_to_ntk_command (const environment::ptr &env) : command (env, "Convert AIG to Ntk (Old style)")
  {
    opts.add_options()
      ( "id", po::value( &id )->default_value( id ), "store id of AIG circuit" )
      ( "new,n" ,                                  "Create a new Network store entry" )
      ;
  }

protected:
  rules_t validity_rules() const;
  bool execute(); 
  log_opt_t log() const;

private:
  unsigned id;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
