// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : ntk_to_aig.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------

#include "ntk_to_aig.hpp"

namespace alice
{

ntk_to_aig_command::rules_t ntk_to_aig_command::validity_rules() const {
  return {
    { [this]() {
	const auto& ntks = env->store<abc::Abc_Ntk_t*>();
	return id < ntks.size();
      }
      , "Id out of range!"  },

      //rule2
    { [this]() {
	const auto& ntks = env->store<abc::Gia_Man_t*>();
	return !ntks.empty();
      }
      , "No Network in the store!"  }

      
      };
}

bool ntk_to_aig_command::execute() {
  auto& ntks = env->store <abc::Abc_Ntk_t *>();
  auto ntk = ntks[id];
  if (ntk == nullptr) return false;

  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  if (ntk == nullptr) return false;
  auto is_seq_ckt = false;
  if ( abc::Abc_NtkLatchNum (ntk) != 0 ) is_seq_ckt = true;
  
  auto dar = abc::Abc_NtkToDar ( ntk, 0, 0 );
  auto gia = abc::Gia_ManFromAig ( dar );
  if (gia == nullptr) return false;
  gia->pName = abc::Abc_UtilStrsav ( ntk->pName );
  if (is_seq_ckt) { assert (gia->nRegs > 0); } 
  
  auto& gias = env->store <abc::Gia_Man_t *>();
  if ( is_set("new") || gias.empty() ) {
    gias.extend();
  }
  gias.current() = gia;
  return true;
}

ntk_to_aig_command::log_opt_t ntk_to_aig_command::log() const  {
  return log_opt_t (
    {
      {"aig_id", id}
    }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
