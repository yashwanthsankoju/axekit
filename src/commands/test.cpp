// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : test.cpp
// @brief  : A placeholder for experimental features  
//           
//           
//------------------------------------------------------------------------------

#include "test.hpp"

namespace alice
{

test_command::rules_t test_command::validity_rules() const {
  return {
    { [this]() { return true; } , "Never fails"  }
  };
}






bool test_command::execute() {
  //std::cout << "----------- TEST ----------\n";

  //---auto& gias = env->store<abc::Gia_Man_t*> ();
  //---auto fs = gias[0];
  //---auto fshat = gias[1];
  //---auto er = error_rate (fs, fshat);
  //---std::cout << "Error Rate = " << er << "\n";
  
  auto& gias = env->store<abc::Gia_Man_t*> ();
  auto gia = gias[0];

  auto yig = axekit::gia_to_yig (gia, abc::Gia_ManObjNum(gia) );

  
  //---auto cnf = abc::Mf_ManGenerateCnf (gia, 8, 0, 0, 0);
  //---char cnffile[] = "GreaterThanCnf.cnf";
  //---char cnffile2[] = "abcCnf.cnf";
  //---auto fReadable = 1;
  //---axekit::write_cnf ( cnf, cnffile, fReadable, abc::Gia_ManPiNum (gia), abc::Gia_ManPoNum (gia) );
  //---abc::Cnf_DataWriteIntoFile (cnf, cnffile2, 1, NULL, NULL);
  
  //std::cout << "LEXSAT Value = " << axekit::solve_max_lexsat (gia) << "\n";
  //std::cout << "Sum of Errors = " << axekit::sum_of_max_values (gia, 100) << "\n";

  //std::cout << "Max Value = "    << experimental_solve_maxlexsat_int (gia) << "\n";

  
  //std::cout << "----------- TEST ----------\n";
  return true;
}

test_command::log_opt_t test_command::log() const  {
  return log_opt_t (
    {
      {"file", file},
      }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
