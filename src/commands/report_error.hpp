// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : report_error.hpp
// @brief  : Report the various error metrics. (error-rate, worst-case, bit-flip)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef REPORT_ERROR_HPP
#define REPORT_ERROR_HPP

#include <commands/axekit_stores.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <cirkit/cudd_cirkit.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>
#include <functions/gia/gia_error_metrics.hpp>

namespace alice {

class report_error_command : public command {
public:
  report_error_command (const environment::ptr &env) : command (env, "Report different approximation error metrics")
  {
    //add_positional_option ("report");
    opts.add_options()
      ( "id1", po::value( &id1 )->default_value( id1 ), "store id of first circuit" )
      ( "id2", po::value( &id2 )->default_value( id2 ), "store id of second circuit" )
      ( "store,s", po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / BDD=1)" )
      ( "all",     "Compute all error metrics :: error-rate, worst-case (chi), average-case, bit-flip (chi)" )
      ( "wc",      "Compute worst-case error" )
      ( "wc_chi",  "Compute worst-case error (BDD Chi method, store=1)" )
      ( "ac",      "Compute average-case error" )
      ( "er",      "Compute error-rate" )
      ( "bf",      "Compute maximum bit-flip error" )
      ( "bf_chi",  "Compute maximum bit-flip error (BDD Chi method. store=1)" )
      ( "mc_exact",    "Compute exact model-counts with sharpSAT (SAT, store=0) Default: error-rate: ckts with PI >= 128 are automatically switched to approx for error-rate. Average-case: default is aXc internal." )
      ( "mc_approx",   "Compute approximate model-counts with CMS (SAT, store=0). Default: error-rate: ckts with PI >= 128 are automatically switched to approx. Average-case: default is aXc interal." )
      ( "aig_wc_threshold",   po::value (&aig_wc_threshold)->default_value( aig_wc_threshold ),
	"SAT threshold for #MaxErrors in average-case error (Only with AIG)" )
      ( "report,r",   po::value (&rpt_file)->default_value( rpt_file ), "Report file to be written" )
      ;
  }

//      ( "all",  po::value( &all_flag )->default_value( all_flag ),
//	"Compute all error metrics (error-rate, worst-case (chi), average-case, bit-flip (chi) )" )
//      ( "wc",  po::value( &wc_flag )->default_value( wc_flag ), "Compute worst-case error" )
//      ( "wc_chi",  po::value( &wc_flag )->default_value( wc_chi_flag ), "Compute worst-case error (Chi method)" )
//      ( "ac",  po::value( &ac_flag )->default_value( ac_flag ), "Compute average-case error" )
//      ( "er",  po::value( &er_flag )->default_value( er_flag ), "Compute error-rate" )
//      ( "bf",  po::value( &bf_flag )->default_value( bf_flag ), "Compute maximum bit-flip error" )
//      ( "bf_chi",  po::value( &bf_chi_flag )->default_value( bf_chi_flag ), "Compute maximum bit-flip error (Chi method)" )


  
protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string rpt_file = "AxC_error_metrics.rpt";
  unsigned id1 = 0;
  unsigned id2 = 1;
  unsigned store = 0;

  // results variables.
  mpfloat error_rate_percent = 0.0;
  boost::multiprecision::uint256_t error_rate = 0u;
  boost::multiprecision::uint256_t wc_error   = 0u;
  boost::multiprecision::uint256_t wc_error_chi   = 0u;
  boost::multiprecision::cpp_dec_float_100 avg_error = 0;
  boost::multiprecision::uint256_t bf_error = 0u;   // this should be a thin wrapper around unsigned.
  boost::multiprecision::uint256_t bf_error_chi = 0u;   // this should be a thin wrapper around unsigned.

  float er_time = -1;
  float wc_time = -1;
  float wc_time_chi = -1;
  float ac_time = -1;
  float bf_time = -1;
  float bf_time_chi = -1;


  bool wc_flag = false;
  bool ac_flag = false;
  bool wc_chi_flag = false;
  bool er_flag = false;
  bool bf_flag = false;
  bool bf_chi_flag = false;
  bool all_flag = false;

  // FROM the LSB, aig_wc_threshold values are guaranteed to be covered by the incremental-max-value SAT calls.
  // e.g. If the max error = 123 and aig_wc_threshold = 5, then it is guranteed that after 123, 5 more
  //      max errors (e.g. 122, 121, 110, 108 etc) are taken care in average case computations.
  // Note: if every combination need to be accomodated (for an exact result),
  //       give aig_wc_threshold as 2^num_output_bits or more.
  // e.g. If the function is 5 bits output, aig_wc_threshold = 31 returns the exact result without approximation.
  boost::multiprecision::uint256_t aig_wc_threshold = 10000;
  
  void write_report();
  void set_flags();
  void clear_all();

};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
