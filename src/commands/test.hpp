// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : test.hpp
// @brief  : Experimental feature testing.
//
//------------------------------------------------------------------------------
#pragma once

#ifndef TEST_HPP
#define TEST_HPP


#include <iostream>
#include <fstream>
#include <sstream>
#include <functional>
#include <cassert>
#include <cstdio>
#include <gmpxx.h>
#include <stack>
#include <vector>
#include <unordered_map>
#include <utility>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/range/numeric.hpp>

#include <commands/axekit_stores.hpp>

#include <cudd.h>
#include <cuddInt.h>
#include <cuddObj.hh>

#define LIN64
#include <aig/aig/aig.h>
#include <base/main/main.h>
#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/abc/functions/gia_to_cirkit.hpp>
#include <ext-libs/cms5/scalmc.h>

#include <cirkit/aig_from_bdd.hpp>
#include <cirkit/bdd_utils.hpp>
#include <cirkit/bitset_utils.hpp>
#include <cirkit/cudd_arithmetic.hpp>
#include <cirkit/cudd_cirkit.hpp>
#include <cirkit/range_utils.hpp>

#include <functions/bdd/bdd_error_metrics.hpp>
#include <functions/bdd/bdd_logical.hpp>
#include <functions/characteristic.hpp>
#include <functions/cirkit_routines.hpp>
#include <utils/common_utils.hpp>
#include <utils/nanotrav/blif_nanotrav_io.hpp>

#include <functions/gia/gia_arithmetic.hpp>
#include <functions/gia/gia_error_metrics.hpp>
#include <functions/gia/gia_lexsat.hpp>
#include <functions/gia/gia_countsat.hpp>
#include <functions/gia/gia_logical.hpp>

#include <misc/util/abc_global.h>
#include <proof/cec/cec.h>
#include <sat/bsat/satSolver.h>
#include <sat/cnf/cnf.h>

#define LN( x ) if ( verbose ) { std::cout << x << std::endl; }

#include <functions/yig/yig_conversion.hpp>


namespace alice {

class test_command : public command {
public:
  test_command (const environment::ptr &env) : command (env, "Experimental feature")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f",   po::value (&file),         "The input file" )
      ( "new,n" ,                                  "Create a new store entry" )
      ;
  }

protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string file;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
