// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : arithmetic.hpp
// @brief  : Addition, subtraction, absolute value of two functions
//
//------------------------------------------------------------------------------
#pragma once

#ifndef ARITHMETIC_HPP
#define ARITHMETIC_HPP

#include <commands/axekit_stores.hpp>
#include <cirkit/cudd_arithmetic.hpp>
#include <vector>

namespace alice {

class arithmetic_command : public command {
public:
  arithmetic_command (const environment::ptr &env) : command (env, "Compute the arithmetic functions")
  {
    opts.add_options()
      ( "id1", po::value( &id1 )->default_value( id1 ), "store id of first circuit" )
      ( "id2", po::value( &id2 )->default_value( id2 ), "store id of second circuit" )
      ( "store,s", po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / BDD=1)" )
      ( "new,n" ,                                "Create a new store entry" )
      ( "add" ,                                  "id1 + id2  : add. CarryOut as MSB" )
      ( "sub" ,                                  "id1 - id2  : BDD:: 2s compliment subtract, BorrowOut as MSB. For AIG:: Always returns the absolute difference" )
      ( "abs" ,                                  "|id1|      : absolute value of id1. MSB function is taken as carry. (id2 is ignored)" )
      ( "max" ,                                  "Maximum value of id1. (id2 is ignored)" )

      ;
  }

protected:
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  unsigned id1 = 0;
  unsigned id2 = 1;
  unsigned store = 0;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
