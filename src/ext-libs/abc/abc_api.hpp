/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file abc_api.hpp
 *
 * @brief ABC interface
 *
 * Interface to ABC
 *
 * @author Arun
 * @author Heinz Riener
 */

#ifndef ABC_API_HPP
#define ABC_API_HPP

#include <iostream>
#include <string>
#include <cassert>

#define LIN64
#include <base/main/main.h>
#include <aig/aig/aig.h>
#include <aig/gia/gia.h>
#include <misc/util/abc_global.h>
#include <sat/cnf/cnf.h>

namespace abc {
void Abc_UtilsPrintHello( Abc_Frame_t * pAbc );
void Abc_UtilsSource( Abc_Frame_t * pAbc );
char * Abc_UtilsGetUsersInput( Abc_Frame_t * pAbc );
void Mf_ManDumpCnf( Gia_Man_t * p, char * pFileName, int nLutSize, int fCnfObjIds, int fAddOrCla, int fVerbose );
Cnf_Dat_t * Mf_ManGenerateCnf( Gia_Man_t * pGia, int nLutSize, int fCnfObjIds, int fAddOrCla, int fVerbose );
Aig_Man_t * Gia_ManToAig( Gia_Man_t * p, int fChoices );
Gia_Man_t * Gia_ManFromAig( Aig_Man_t * p );
Abc_Ntk_t * Abc_NtkFromAigPhase( Aig_Man_t * pMan );
Aig_Man_t * Abc_NtkToDar( Abc_Ntk_t * pNtk, int fExors, int fRegisters );
}


namespace axekit {
bool start_abc();
void stop_abc();
bool is_abc_started();

void write_verilog ( abc::Abc_Ntk_t *ntk, char *file );
void write_verilog ( abc::Gia_Man_t *gia, char *file );
void write_verilog ( abc::Abc_Ntk_t *ntk, const std::string &file );
void write_verilog ( abc::Gia_Man_t *gia, const std::string &file );

// do the below in 2 steps. Else errors will not be easy to understand.
inline abc::Gia_Man_t * read_aiger ( const std::string &file ) {
  char name[] = "golden";
  auto gia = abc::Gia_AigerRead ( (char *)file.c_str(), 1, 1, 1 );
  gia->pName = abc::Abc_UtilStrsav ( name );
  assert ( gia != nullptr );
  if ( gia->nRegs > 0 ) {
    assert (false && "Golden circuit is sequential!");
  }
  return gia;
}

inline abc::Abc_Ntk_t * convert_gia_to_ntk ( abc::Gia_Man_t *gia ) {

  /*
  // below is the actual procedure. But its a segfault.
  // hence taking this route.
  auto aigman = abc::Gia_ManToAig (gia, 0);
  auto ntk =  abc::Abc_NtkFromAigPhase ( aigman );
  */

  std::string ntk_file = "/tmp/tmp_aig.aig";
  abc::Gia_AigerWrite ( gia, (char*)ntk_file.c_str(), 1, 0 );    
  auto ntk = abc::Io_ReadAiger ( (char *)ntk_file.c_str(), 1 );
  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  ntk->pName = abc::Abc_UtilStrsav ( gia->pName );
  return ntk;
}


} // axekit




#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
