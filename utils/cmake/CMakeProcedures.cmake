include( CMakeParseArguments )

set(
  CMakeCommonUtils_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}"
  CACHE PATH "Where CMakeCommonUtils finds its own files"
  FORCE
)
mark_as_advanced( CMakeCommonUtils_DIRECTORY )

#----------------------------------------------------------------------------------
# Function to add a new library.
# Common Usage  ::
#  new_library (
#        NAME         <name> - the name of the library
#        LIB_DIRS     <dir>  - list of directories to search for cpp files
#        EXTRA_LIBS   <lib>  - list of extra libs to link with
#        )
# Simple Example ::
#   new_library (
#        NAME         myLib
#        LIB_DIRS     ${TOP-LIB}/src/my_dir1 ${TOP-LIB}/src/my_dir2
#        EXTRA_LIBS   ${Boost_TIMER_LIBRARIES}
#        )
# 
function ( new_library )
  cmake_parse_arguments(
    "arg"
    ""
    "NAME"
    "SOURCES;LIB_DIRS;EXTRA_LIBS;INCLUDE;DEFINE"
    ${ARGN}
  )

  if( DEFINED arg_UNPARSED_ARGUMENTS )
    message( FATAL_ERROR "invalid arguments passed to new_library: ${arg_UNPARSED_ARGUMENTS}" )
  endif( )

  if( NOT DEFINED arg_NAME )
    message( FATAL_ERROR "new_library requires a NAME: add_axekit_library( NAME abc ...)" )
  endif( )

  if( (NOT arg_LIB_DIRS) AND (NOT DEFINED arg_SOURCES) )
    message( FATAL_ERROR "Adding lib :: ${arg_NAME} NOT possible. Specify LIBS_DIRS for the location to pick the source files in new_library [ recurses through all sub-dirs under LIBS_DIRS except any folder by name *programs* ]" )
  endif( )

  if( DEFINED arg_LIB_DIRS )
    message( "Adding lib :: ${arg_NAME} :: source files from <${arg_LIB_DIRS}>/** [ recurses through all sub-dirs except any folder by name *programs* ]" )
    foreach( curr_dir ${arg_LIB_DIRS} )
      subdirlist ( sub_dirs ${curr_dir} )
      #file ( GLOB sub_dirs ${curr_dir}  )
      foreach( dir ${sub_dirs} )
	if("${dir}" MATCHES "programs")
	  continue()
	endif()
	file( GLOB_RECURSE cpp_files ${dir}/*.cpp )
	list( APPEND arg_SOURCES ${cpp_files} )
      endforeach( )
    endforeach( )
  endif( )

  add_library( ${arg_NAME} SHARED
               ${CMakeCommonUtils_DIRECTORY}/nothing.cpp ${arg_SOURCES} )
  set_property( TARGET ${arg_NAME} PROPERTY POSITION_INDEPENDENT_CODE on )

  if( DEFINED arg_EXTRA_LIBS )
    target_link_libraries( ${arg_NAME} PUBLIC ${arg_EXTRA_LIBS} )
  endif( )

  if( DEFINED arg_INCLUDE )
    target_include_directories( ${arg_NAME} ${arg_INCLUDE} )
  endif( )

  if( DEFINED arg_DEFINE )
    target_compile_definitions( ${arg_NAME} ${arg_DEFINE} )
  endif( )

endfunction( )
#----------------------------------------------------------------------------------
# Function to add a new executable.
# Common Usage ::
#    new_program (
#       NAME   <name> - name of the program [Source file TOP-DIR/src/programs/<name>.cpp]
#       LIBS   <libs> - list of libraries
#       )
# Common Example ::
#    new_program (
#       NAME   runMe 
#       LIBS   ${abc_LIBRARIES} ${yosys_LIBRARIES} ${Boost_TIMER_LIBRARIES}
#       )
# Note: TOP-DIR/src/programs/runMe.cpp should exist with int main(){..})
#
function( new_program )
  cmake_parse_arguments(
    "arg"
    ""
    "NAME"
    "SOURCES;AUTO_DIRS;LIBS;INCLUDE"
    ${ARGN}
  )

  if( DEFINED arg_UNPARSED_ARGUMENTS )
    message( FATAL_ERROR "invalid arguments passed to new_program: ${arg_UNPARSED_ARGUMENTS}" )
  endif( )

  if( NOT DEFINED arg_NAME )
    message( FATAL_ERROR "new_program requires a NAME: new_program( NAME abc ...)" )
  endif( )

  #---if( (NOT arg_AUTO_DIRS) AND (NOT DEFINED arg_SOURCES) )
  #---  message( FATAL_ERROR "program ${arg_NAME} specifies neither SOURCES nor AUTO_DIRS." )
  #---endif( )

  if( NOT DEFINED arg_SOURCES )
    set (arg_SOURCES ${CMAKE_SOURCE_DIR}/src/programs/${arg_NAME}.cpp )
  endif( )
  
  if( DEFINED arg_AUTO_DIRS )
    foreach( dir ${arg_AUTO_DIRS} )
      file( GLOB_RECURSE files ${dir}/*.cpp )
      list( APPEND arg_SOURCES ${files} )
    endforeach( )
  endif( )

  add_executable( ${arg_NAME} ${arg_SOURCES} )

  if( DEFINED arg_LIBS )
    target_link_libraries( ${arg_NAME}  ${arg_LIBS} )
  endif( )

  message ("Adding program :: ${arg_NAME} :: libs - ${arg_LIBS} :: source - ${arg_SOURCES}")
  
  if( DEFINED arg_INCLUDE )
    target_include_directories( ${arg_NAME} ${arg_INCLUDE} )
  endif( )

endfunction( )
#----------------------------------------------------------------------------------
